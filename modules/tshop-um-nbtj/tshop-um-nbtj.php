<div class="tb-module tshop-um tshop-um-nbtj">
	<?php
	global $c, $t, $srd, $itemManager, $uriManager;
	function discountPrice($item) {
		if ($item->discountPrice && $item->discountPrice != $item->price) {
			$discountPrice = number_format($item->discountPrice, 3, ".", "");//格式化数字（取三位小数）
		} else {
			$discountPrice = NULL;
		}
		$discountPrice = substr($discountPrice, 0, -1);//截取两位小数
		return $discountPrice;
	}

	/**
	 * //通过获取输入的JSON商品类目对象,最终获得不重复的商品对象
	 * @param $selectCategoryIn
	 * @param $sortType
	 * @param $count
	 * @return array
	 * //获取类目选择器选择的类目,生成不重复的所有类目宝贝的ID数组;然后查询
	 */
	function getItemsFromCategoryS($selectCategoryIn, $sortType, $count) {
		global $itemManager;
		$categoryIds = array();
		$itemsTs = array();
		$itemsAll = array();
		$itemIds = array();
		$items = array();
		if ($selectCategoryIn) {
			$categoryArr = json_decode($selectCategoryIn, TRUE);
			foreach ($categoryArr as $ids) {
				if ($ids['childIds'] == '') {
					$categoryIds[] = $ids['rid'];
				} else {
					$childIds = explode(',', $ids['childIds']);
					foreach ($childIds as $childId) {
						$categoryIds[] = $childId;
					}
				}
			}
			foreach ($categoryIds as $categoryId) {
				$itemsTs[] = $itemManager->queryByCategory($categoryId, $sortType, 20);
			}
		}
		if (count($itemsTs) > 0) {
			foreach ($itemsTs as $itemsT) {
				foreach ($itemsT as $item) {
					$itemsAll[] = $item;
				}
			}
		}
		if (count($itemsAll) > 0) {
			foreach ($itemsAll as $item) {
				$itemIds[] = $item->id;
			}
			$itemIds_tmp = array_flip(array_flip($itemIds));
			$itemsQs = $itemManager->queryByIds($itemIds_tmp, $sortType);
			for ($i = 0; $i < $count; $i++) {
				$items[$i] = $itemsQs[$i];
			}
		}
		return $items;
	}

	$p = $c->inputIfoArr('p', 30);
	$pt = $c->inputIfoArr('t', 10);
	$layerType = $p[10];
	$padIng = 10;
	$numOLine = $p[8];
	$defCount = 9;
	$sortType = $p[6];
	switch ($layerType) {
		case 'm950':
			switch ($numOLine) {
				case 5:
					$picSize = 160;
					break;
				case 4:
					$picSize = 210;
					break;
				case 3:
					$picSize = 290;
					break;
			}
			break;
		case 'm1920':
			switch ($numOLine) {
				case 5:
					$picSize = 310;
					break;
				case 4:
					$picSize = 400;
					break;
				case 3:
					$picSize = 480;
					break;
			}
			break;
	}
	$itemsBoxWidth = ($picSize + $padIng * 2) * $numOLine + $numOLine - 1;
	$count = $srd->isInt($p[7]) ? $p[7] : $defCount;
	if ($count % $numOLine != 0) {
		$count = $count + $numOLine - ($count % $numOLine);
	}
	$items = $itemManager->queryByKeyword('', $sortType, $count);
	switch ($p[2]) {
		case 1:
			if ($p[3]) {
				$ids = $c->strEP($p[3], ',');
				$items = $itemManager->queryByIds($ids, $sortType);
			}
			break;
		case 2:
			$items_this = getItemsFromCategoryS($p[4], $sortType, $count);
			if (count($items_this) > 0) {
				$items = $items_this;
			}
			break;
		case 3:
			if ($p[5]) {
				$items = $itemManager->queryByKeyword($p[5], $sortType, $count);
			}
			break;
	}
	?>
	<div class="box <?= $layerType ?>" <?= echoStyle(array(
		$srd->sMarginTop($c->strEP($p[0], '|')[0]),
		$srd->sMarginBottom($c->strEP($p[0], '|')[1]),
	)) ?>>
		<?php
		$pa = $c->inputIfoArr('pa', 10);
		if ($pa[0]) { ?>
			<div class="topTitle" <?= echoStyle(array(
				$layerType == 'm1920' ? $srd->sMarginLeft(0) : NULL,
			)) ?>>
				<div class="bg_image" <?= echoStyle(array($srd->sBgImage($p[25]))) ?>></div>
				<div class="bg_mask"></div>
				<span class="t_text1" <?= echoStyle(array(
					$srd->sColor($p[27]),
					"font-family: arial,sans-serif",
				)) ?>><?= $p[26] ?></span>
				<span class="title_text" <?= echoStyle(array(
					$srd->sColor($p[29]),
					"font-family: 'Microsoft Yahei'",
				)) ?>><?= $p[28] ?></span>
			</div>
		<? } ?>
		<div class="itemsBox"<?= echoStyle(array($srd->sWidth($itemsBoxWidth))) ?>>
			<ul class="itemsLists">
				<? for ($num = 0, $i = 0;
						$num < $count;
						$num++) {
					$i = $num;
					if ($i >= count($items)) {
						$i = 0;
					}
					if ($items[0]->exist) {
						//$id = $items[$i]->id;
						if ($items[$i]->discountPrice && $items[$i]->discountPrice != $items[$i]->price) {
							$realPrice = $items[$i]->discountPrice;
						} else {
							$realPrice = $items[$i]->price;
						}
						$price = number_format($items[$i]->price, 2, '.', '');
						$discountPrice = discountPrice($items[$i]);
						$realPrice = $discountPrice ? $discountPrice : $price;
						//$soldCount = $items[$i]->soldCount;
						$detaiLURI = $uriManager->detailURI($items[$i]);
						$title = $items[$i]->title;
						$picture = $items[$i]->getPicUrl($picSize);
					} else {
						$price = 0;
						$discountPrice = 0;
						$realPrice = '0.00';
						$detaiLURI = '#';
						$title = '没有选择宝贝,请选择宝贝';
						$picture = 'https://img.alicdn.com/imgextra/i1/2667166845/TB2F3ZYkXXXXXaZXXXXXXXXXXXX_!!2667166845.png';
					}
					?>
					<li class="item_infoAll <?= 'li' . $num ?>" <?= echoStyle(array(
						$srd->sMarginTop($num < $numOLine ? NULL : 1),
						$srd->sMarginLeft($num % $numOLine == 0 ? NULL : 1),
						is_int($padIng) ? 'padding:' . $padIng . 'px' : NULL,
					)); ?>>
						<a class="imgLink" target="<?= $p[1] ?>"
						   href="<?= $detaiLURI ?>"<?= echoStyle(array(
							$srd->sWidth($picSize),
							$srd->sHeight($picSize),
						)) ?>>
							<span class="state_gg state_s1"></span>
							<span class="state_gg state_s2"></span>
							<span class="state_gg state_s3"></span>
							<span class="state_gg state_s4"></span>
							<img class="item_img <?
							if ($p[11] == 0) {
								echo ' circle';
							}
							if ($p[12] == 0) {
								echo ' rotate';
							}
							?>" src="<?= $picture ?>" alt="<?= $title ?>"
								 title="<?= $title ?>"/>
							<? if ($c->isInChecks(0, $p[9]) !== NULL) { ?>
								<div class="title">
									<span <?= echoStyle(array($srd->sWidth($picSize), $srd->sColor($p[14]), $p[15],)) ?>
										class="item_title"><?= $title ?></span>
									<div class="t-mask"></div>
								</div>
							<? } ?>
							<? if ($p[13] == 0) { ?>
								<div class="mask"></div>
							<? } ?>
						</a>
						<div class="info-box">
							<div class="itemPrice">
								<? if ($c->isInChecks(1, $p[9]) !== NULL && $discountPrice) { ?>
									<span class="oldPrice"><?= $price ?></span>
								<? } ?>
								<? if ($c->isInChecks(2, $p[9]) !== NULL) { ?>
									<span class="r_dl" <?= echoStyle(array(
										$srd->sColor($p[17]),
										$p[18],
									)) ?>><?= $p[16] ?></span>
									<span class="realPrice" <?= echoStyle(array(
										$srd->sColor($p[19]),
										$p[20],
									)) ?>><?= $realPrice ?></span>
								<? } ?>
							</div>
							<div class="item_buyBar" <?
							if (!$c->isInChecks(3, $p[9]) || !$c->isInChecks(4, $p[9])) {
								$item_buyBarWidth = 70;
							} else {
								$item_buyBarWidth = NULL;
							}
							echo echoStyle(array(
								$srd->sWidth($item_buyBarWidth),
								$srd->sTop($c->strEP($p[24], ',')[0]),
								$srd->sLeft($c->strEP($p[24], ',')[1]),
							));
							?>>
								<? if ($c->isInChecks(3, $p[9]) !== NULL) { ?>
									<div class="add_cart">
										<a <?= echoStyle(array(
											$srd->sColor($p[21]),
											$p[23],
										)) ?>
											target="<?= $p[1] ?>" href="<?= $detaiLURI ?>" class="J_CartPluginTrigger">
											<?php if ($pt[0]) { ?>
												<span class="b_text">加入购物车</span>
												<span class="bgc" <?= echoStyle(array($srd->sBgColor($p[22]))) ?>></span>
											<?php } else { ?>
												<span class="cart_img"></span>
											<?php } ?>
										</a>
									</div>
								<? } ?>
								<? if ($c->isInChecks(4, $p[9]) !== NULL) { ?>
									<div class="buy_now">
										<a <?= echoStyle(array(
											$srd->sColor($p[21]),
											$p[23],
										)) ?>
											target="<?= $p[1] ?>" href="<?= $detaiLURI ?>">
											<?php if ($pt[0]) { ?>
												<span class="b_text">立即购买</span>
												<span class="bgc" <?= echoStyle(array($srd->sBgColor($p[22]))) ?>></span>
											<?php } else { ?>
												<div class="b_img"></div>
											<?php } ?>
										</a>
									</div>
								<? } ?>
							</div>
						</div>
					</li>
				<? } ?>
			</ul>
		</div>
	</div>
</div>
