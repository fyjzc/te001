<?php
//获取XML数据
function getMod($m = 'm',$in = 20) {
	global $_MODULE;
	$p = array();
	for ($i = 0;$i <= $in;$i++) {
		$p[$i] = $_MODULE[$m . $i];
	}
	return $p;
}

//dom样式
function sStyle($v,$dl = '|') {
	$tr = explode($dl,$v);
	return $v ? ' style="' . implode(';',$tr) . '"' : '';
}

function isInt($v) {
	return intval($v) . '' == $v ? 1 : 0;
}

function isColor($v) {
	preg_match_all('/#/',$v,$count);
	$strLen = strlen($v);
	$value = str_replace('#','',$v);
	if (is_numeric(hexdec($value)) && $v[0] == '#' && count($count[0]) == 1 && ($strLen == 4 || $strLen == 7)) {
		return 1;
	}
	return 0;
}

function isUrl($v) {
	return ((strpos($v,'http://') === 0) || (strpos($v,'https://') === 0)) ? 1 : 0;
}

function isImage($v) {
	return isUrl($v) && (strpos($v,'.png') || strpos($v,'.jpg') || strpos($v,'.gif')) ? 1 : 0;
}

function sMb($v) {
	return isInt($v) ? 'margin-bottom:' . $v . 'px|' : '';
}

function sBgIO($v) {
	return 'background:url(https:' . $v . ') center center no-repeat|';
}

function sBgI($v) {
	return isImage($v) ? 'background:url(' . $v . ') center center no-repeat|' : '';
}

function sColor($v) {
	return isColor($v) ? 'color:' . $v . '|' : '';
}

function sBDColor($v) {
	return isColor($v) ? 'border-color:' . $v . '|' : '';
}

function sFontFamily($v) {
	return 'font-family:\'' . $v . '\'|';
}

function sFontSize($v) {
	return isInt($v) ? 'font-size:' . $v . 'px|' : '';
}

function sBgC($v) {
	return isColor($v) ? 'background-color:' . $v . '|' : '';
}

function sWidth($v) {
	return isInt($v) ? 'width:' . $v . 'px|' : '';
}

function sHeight($v) {
	return isInt($v) ? 'height:' . $v . 'px|' : '';
}

function sMl($v) {
	return isInt($v) ? 'margin-left:' . $v . 'px|' : '';
}

function sTop($v) {
	return isInt($v) ? 'top:' . $v . 'px|' : '';
}

//收藏店铺or宝贝
function getFav($favText,$favItemid,$favData = 'shop',$favClass = "yunYiFav") {
	global $uriManager;
	$sc_title = '';
	$sc_url = '';
	if ($favData == 'shop') {
		$sc_url = $uriManager->favoriteLink();
		$sc_title = "点此收藏本店铺";
	} elseif ($favData == 'item') {
		$sc_url = "http://favorite.taobao.com/popup/add_collection.htm?id=" . $favItemid . "&itemtype=1&scjjc=1";
		$sc_title = "点此收藏这个宝贝";
	}
	$sc = '<a class="J_TokenSign ' . $favClass . '" title="' . $sc_title . '" href="' . $sc_url . '" target="_blank">' . $favText . '</a>';
	return $sc;
}

//生成二维码
function getErcode($erId,$widthHeight = "90",$type = "1") {
	switch ($type) {
		case 1:
			$code = 'type=ci&item_id=' . $erId . '&v=1';
			break;
		case 2:
			$code = 'v=1&type=bi&item_id=' . $erId;
			break;
		case 3:
			$code = 'type=cs&shop_id=' . $erId . '&v=1';
			break;
		case 4:
			$code = 'v=1&type=bs&shop_id=' . $erId;
			break;
		case 5:
			$code = 'type=we&we_id=' . $erId . '&v=1';
			break;
		default:
			$code = 'type=ci&item_id=' . $erId . '&v=1';
	}
	$imgsrc = 'http://gqrcode.alicdn.com/img?' . $code . '&w=' . $widthHeight . '&h=' . $widthHeight;
	return $imgsrc;
}

function in_check($h,$check,$dt = "@_@") {
	$cr = explode($dt,$check);
	return in_array($h,$cr);
}

$fms = array('微软雅黑','黑体','宋体','楷体','华文行楷','隶书','幼圆','Arial');
$m = getMod('m',13);
$mt = getMod('mt',1);
?>
<div class="tb-module tshop-um tshop-um-ft" <?= sStyle(sFontFamily($fms[0])) ?>>
	<div class="bd">
		<? if (in_check(1,$m[1])) { ?>
			<div class="title">
				<div class="tit1"
					<?= sStyle(sFontFamily($m[3]) . sFontSize($m[4]) . sColor($m[5])) ?>><?= $m[2] ?></div>
				<div class="tit2"
					<?= sStyle(sFontFamily($m[7]) . sFontSize($m[8]) . sColor($m[9])) ?>><?= $m[6] ?></div>
			</div>
		<? } ?>
		<?php
		global $uriManager,$_shop;
		$def_uri = $uriManager->searchURI();
		$cateImages = explode('|',$m[10]);
		$cateUrls = explode('|',$m[11]);
		$cateTits = explode('|',$m[12]);
		$cateColors = explode('|',$m[13]);
		$cateTitsDef = array('外套区','针织区','裙装区','下装区');
		?>
		<? if (in_check(2,$m[1])) { ?>
			<div class="fl">
				<? for ($i = 0;$i < 4;$i++) {
					$style_ml = '';
					if ($i == 0) {
						$style_ml = 0;
					} elseif ($i == 3) {
						$style_ml = 22;
					}
					?>
					<div class="cateBox cate<?= $i ?>" <?= sStyle(sMl($style_ml)) ?>>
						<div class="img" <?= sStyle(sBgI($cateImages[$i])) ?>>
							<div class="in" <?= sStyle(sBgI($cateImages[$i])) ?>></div>
						</div>
						<div class="txt">
							<div class="c_tit" <?= sStyle(sColor($cateColors[$i])) ?>><?= $cateTits[$i] ? $cateTits[$i] : $cateTitsDef[$i] ?></div>
							<div class="b_img"></div>
						</div>
						<a href="<?= $cateUrls[$i] ? $cateUrls[$i] : $def_uri ?>" <?= $m[0] ?>></a>
					</div>
				<? } ?>
			</div>
		<? } ?>
		<? if (in_check(3,$m[1])) { ?>
			<div class="jb">
				<div class="jb_txt1"><a href="https://shop<?= $_shop->id ?>.taobao.com" <?= $m[0] ?>>返回首页</a></div>
				<div class="jb_line"></div>
				<div class="jb_txt2" <?= sStyle(sFontFamily($fms[7])) ?>>BACK TO HOME</div>
				<div class="coll">
					<div class="ico1"></div>
					<div class="fav"><?= getFav('收藏本店BOOKMARK',0,'shop','favTxt'); ?></div>
					<div class="pot">.</div>
					<div class="erw">店铺二维码</div>
					<div class="ico2">
						<div class="er_img"><img src="<?= getErcode($_shop->id,90,3) ?>" alt="二维码"></div>
					</div>
				</div>
				<div class="jb_img"></div>
			</div>
		<? } ?>
		<?= $mt[0] ?>
	</div>
</div>
