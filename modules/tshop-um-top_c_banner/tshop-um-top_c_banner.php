<?php
$p = $c->inputIfoArr('c', 39);
$n1 = $c->inputIfoArr('cn', 1);
$target = $c->_r_sm_3($n1[0] == 0, $t->target[0], $t->target[1]);
$p0 = $t->setStyle(array(($p[0] == 0 ? 1 : 0) => "font-family:'微软雅黑','Microsoft Yahei',sans-serif",));
$p2 = $t->setStyle(array($p[2] . '0' => "background-image:url($p[2])",));
$p3 = $t->setATag(array($p[3], $target, 3 => "height:120px;width:950px",));
//判断用户选择的店招模式,0->图片店招,1->图文店招
if (!$p[1]) {
	$p1 = <<<EOOT
{$p3[0]}
<div class="headPic" {$p2}></div>
{$p3[1]}
EOOT;
} else {
	$p5 = $c->_r_sm_3($p[5], $p[5], $_shop->title);
	$pr8 = $c->strEP($p[8], '|');
	$pr10 = $c->strEP($p[10], '|');
	$pr11 = $c->strEP($p[11], '|');
	$p12a13 = $c->_r_sm($p[12], ' bold-none ') . $c->_r_sm($p[13], ' shadow-none ');
	$p14 = $c->_r_sm($p[14], ' bold-none ');
	$pr7a8 = array(sT1(($p[7] == 1 || $pr8) ? 1 : null, "width:auto"), sT1($pr8[0], "top:{$pr8[0]}px"), sT1($pr8[1], "left:{$pr8[1]}px"),);
	$p7a8 = $t->setStyles($pr7a8);
	$pr9a10 = array(sT1($p[9], "margin-top:{$p[9]}px"), sT1($pr10[1], "color:$pr10[1]"), sT1($pr11[1], "font-size:{$pr11[1]}px"),);
	$p9a10 = $t->setStyles($pr9a10);
	$pr10s = array(sT1($pr10[0], "color:$pr10[0]"), sT1($pr11[0], "font-size:{$pr11[0]}px"),);
	$p10s0 = $t->setStyles($pr10s);
	$p6 = $c->_r_sm($p[6], "<div class=\"tagText {$p14}\" {$p9a10}><span class=\"s2\">{$p[6]}</span></div>");
	$p1 = <<<EOOT
<div class="head">
            <div class="text" {$p7a8}>
                <div class="shopText {$p12a13}" {$p10s0}>
                    <span class="s1"></span>
                    <span class="s2">{$p5}</span>
                </div>
                {$p6}
            </div>
        </div>
EOOT;
}
//店招图文功能结束
//店铺LOGO
$p15 = 'assets/images/banner-logo.gif';
$pr18 = $c->strEP($p[18], ',');
$pr17a18 = array(sT1($p[17], "height:{$p[17]}px"), sT1($pr18[0], "left:{$pr18[0]}px"), sT1($pr18[1], "top:{$pr18[1]}px"),);
$p17a18 = $t->setStyles($pr17a18);
switch ($p[15]) {
	case 0:
		break;
	case 1:
		$p15 = $_shop->shopLogo;
		break;
	case 2:
		$p15 = $c->_r_sm_3($p[16], $p[16], $p15);
		break;
	case 3:
		$p15n = 'd-n';
		break;
}
$pLogo = "<div class=\"logo $p15n\" $p17a18><img src=\"$p15\" alt=\"店铺LOGO\"></div>";
//店铺LOGO结束
//店铺公告
$pr20 = $c->strEP($p[20], '#');
$pr21 = $c->strEP($p[21], '#');
$pr22 = array(array($p[19] == 1 ? 1 : null, "bottom:-21px"), array($p[19] == 1 ? 1 : null, "left: 570px"), array($p[19] == 2 ? 1 : null, "display:none"), sT1($srd->isInt($p[22]), $srd->sWidth($p[22])),);
$p22 = $t->setStyles($pr22);
$pr20lis = array();
$pr20lin = array();
if ($p[20]) foreach ($pr20 as $k => $v) {
	$pr23 = array(array($p[23], "color:{$p[23]}"),);
	$p23 = $t->setStyles($pr23);
	$p21a = $t->setATag(array($pr21[$k] ? $pr21[$k] : '#', $target));
	$pr20lis[$k] = "<li>{$p21a[0]}<span {$p23}>{$v}</span>{$p21a[1]}</li>";
	$pr20lin[$k] = "<li class=\"" . ($k == '0' ? 'selected' : null) . "\">" . ($k + 1) . "</li>";
}
$p20lis = implode($pr20lis);
$p20lin = implode($pr20lin);
$pNotice = <<<EOOT
<div class="sld" {$p22}>
			<div class="J_TWidget" data-widget-type="Slide"
			     data-widget-config="{'effect':'scrolly','easing':'bounceOut','duration':'0.5','interval':3,'navCls':'notice_nav','contentCls':'notice_ct','activeTriggerCls':'selected'}">
				<ul class="notice_ct">{$p20lis}</ul>
				<ul class="notice_nav">{$p20lin}</ul>
			</div>
		</div>
EOOT;
//公告结束
//自定义热点
$p24 = $c->_r_sm($p[24] == 1 ? 1 : null, "style=\"display:none;\"");
$pr25_a = $c->ipStr2Arr2($p[25], '|', ',');
$pr26 = $c->strEP($p[26], '|');
$pHots = array();
if ($p[25]) foreach ($pr25_a as $hotNum => $hots) {
	$hotStyles = array(sT1($hots[0], "width:{$hots[0]}px"), sT1($hots[1], "height:{$hots[1]}px"), sT1($hots[1], "line-height:{$hots[1]}px"), sT1($hots[2], "top:{$hots[2]}px"), sT1($hots[3], "left:{$hots[3]}px"),);
	$hotStyle = $t->setStyles($hotStyles);
	$p26 = $t->setATag(array($pr26[$hotNum] ? $pr26[$hotNum] : '#', $target));
	$i = $hotNum + 1;
	$pHots[$hotNum] = "$p26[0]<div class=\"hotDot hotView\" {$hotStyle}><h1 {$p24}>热点{$i}</h1></div>$p26[1]";
}
$hotDot = implode($pHots);
//热点结束
$pr27 = array();
for ($i = 1; $i < 7; $i++) {
	$pr27[$i] = $c->isInChecks($i, $p[27]);
}
if ($pr27[1]) {
	$p28Checks = $c->check_arr($p[28]);
	foreach ($p28Checks as $p28v) {
		if ($p28v != 4) {
			$p28s[] = "<div class=\"icon icon{$p28v}\"></div>";
		} elseif ($p28v = 4) {
			$p28s[] = "<a class=\"h-collect J_TokenSign\"
href=\"{$uriManager->favoriteLink()}\" target=\"_blank\" title=\"收藏本店\"><div class=\"icon icon{$p28v}\"></div></a>";
		}
	}
	$p28 = implode($p28s);
	$custom = $custom . "<div class=\"icons\">{$p28}</div>";
}
if ($pr27[2]) {
	$ps29 = $c->strEP($p[29], '|');
	$pr29 = array(sT1($ps29[0], "left:{$ps29[0]}px"), sT1($ps29[1], "top:{$ps29[1]}px"),);
	$p29 = $t->setStyles($pr29);
	$search = <<<EOOT
<div class="search" {$p29}>
<div class="search_img"></div>
<form action="{$uriManager->searchURI()}" method="post" target="{$target}">
<label class="text_in"><input type="text" name="keyword"></label>
<label class="btn"><input type="submit" value=""></label>
</form>
</div>
EOOT;
	$custom = $custom . $search;
}
//搜索结束
if ($pr27[3]) {
	$pr30 = $c->strEP($p[30], ',');
	$pr30s = array(sT1($pr30[1], "left:{$pr30[1]}px"), sT1($pr30[2], "top:{$pr30[2]}px"),);
	$p30 = $t->setStyles($pr30s);
	$sharePic = "<div class=\"share_ico\"></div>";
	if ($pr30[0]) {
		$sharePic = "<img src=\"$pr30[0]\" alt=\"分享店铺\">";
	}
	$share = <<<EOOT
<div {$p30} class="share sns-widget"
data-sharebtn='{type:"shop",key:"{$_shop->id}",title:"分享店铺",comment:"{$_shop->title}",pic:"",client_id:68,skinType:3,isShowFriend:false}'>
{$sharePic}</div>
EOOT;
	$custom = $custom . $share;
}
//分享结束
if ($pr27[4]) {
	$pr31 = $c->strEP($p[31], '|');
	$p31link = $t->setATag(array($pr31[1], $target));
	$nextDay = date("Y-m-d H:i:s", time() + (24 * 60 * 60));
	$p32 = $c->_r_sm_3($p[32], $p[32], $nextDay);
	$pr34 = $c->strEP($p[34], '|');
	$p34titleP = array(sT1($pr34[0], "left:{$pr34[0]}px"), sT1($pr34[1], "top:{$pr34[1]}px"),);
	$p34titlePos = $t->setStyles($p34titleP);
	$p34textC = $t->setStyles(array(array($pr34[2], "color:{$pr34[2]}")));
	$p34numS = array(array($pr34[3], "color:{$pr34[3]}"), array($pr34[5], "background:url({$pr34[5]}) 0 0 no-repeat"),);
	$p34numC = $t->setStyles($p34numS);
	$p34etC = $t->setStyles(array(array($pr34[4], "color:{$pr34[4]}")));
	$countText = $c->_r_sm_3($pr31, "{$p31link[0]}<div class=\"text\" {$p34textC}><p>{$pr31[0]}</p></div>{$p31link[1]}", "<div class=\"text\" {$p34textC}><p>距XX活动结束还有:</p></div>");
	$p33 = $c->_r_sm_3($p[33], "<div class=\"ks-countdown-end\" {$p34etC}><span>{$p[33]}</span></div>", "<div class=\"ks-countdown-end\" {$p34etC}><span>活动已结束!</span></div>");
	$countdown = <<<EOOT
<div {$p34titlePos} class="countdown J_TWidget" data-widget-type="Countdown"
		     data-widget-config="{'endTime':'{$p32}','interval':1000,'timeRunCls':'.ks-countdown-run',
'timeUnitCls':{'d': '.ks-d','h': '.ks-h','m': '.ks-m','s': '.ks-s','i': '.ks-i'},'minDigit':1,'timeEndCls':'.ks-countdown-end'}">
			$countText
			<div class="ks-countdown-run">
				<b class="ks-d" {$p34numC}></b><span>天</span><b class="ks-h" {$p34numC}></b><span>时</span><b
				class="ks-m" {$p34numC}></b><span>分</span><b class="ks-s" {$p34numC}></b><span>秒</span>
			</div>{$p33}
		</div>
EOOT;
	$custom = $custom . $countdown;
}
//活动结束
function _price($item) {
	if ($item->discountPrice && $item->discountPrice != $item->price) {
		$discountPrice = number_format($item->discountPrice, 3, ".", "");//格式化数字（取三位小数）
	} else {
		$discountPrice = number_format($item->price, 3, ".", "");//同上
	}
	return substr($discountPrice, 0, -1);//截取两位小数
}

if ($pr27[5]) {
	$pr36 = $c->strEP($p[36], ',');
	$pr36def = array('爆款', '热销', '新品',);
	$pr36 = $c->_r_sm_3($p[36], $pr36, $pr36def);
	$pr37 = $c->strEP($p[37], ',');
	$item_position = $t->setStyles(array(sT1($pr37[0], "left:{$pr37[0]}px"), sT1($pr37[1], "top:{$pr37[1]}px")));
	$item_titleColor = $t->setStyles(array(array($pr37[2], "color:{$pr37[2]}")));
	$item_priceColor = $t->setStyles(array(array($pr37[3], "color:{$pr37[3]}")));
	$item_tagColor = $t->setStyles(array(array($pr37[4], "color:{$pr37[4]}")));
	$item_borderColor = $t->setStyles(array(array($pr37[5], "background-color:{$pr37[5]}")));
	$Carousel_config = "data-widget-config=\"{'effect':'scrollx','easing':'backOut','circular':true,'autoplay':true,
'contentCls':'content','navCls':'nav','activeIndex':0,'activeTriggerCls':'selected','triggerType':'mouse',
'duration':0.8,'interval':4,'delay':0,'prevBtnCls': 'prev_g','nextBtnCls': 'next_g'}\"";
	$itemPics = array();
	$p35_navLis = array();
	$p35_tag2 = mt_rand(0, 3) == 1 ? 'tag2' : '';
	$items = $itemManager->queryByKeyword('', '_hotsell', 6);
	if ($p[35]) {
		$items = $itemManager->queryByIds($c->strEP($p[35], ','), "hotsell"); //按照ID获取宝贝对象;
	}
	foreach ($items as $ik => $item) {
		$p35_tag2 = mt_rand(1, 2) == 1 ? 'tag2' : '';//随机标签
		$tagIndex = mt_rand(0, count($pr36) - 1);//随机标签数组序列
		$itemUrl = $uriManager->detailURI($item);
		$itemPic = $item->getPicUrl(80);
		$itemTitle = str_replace('\\n', '', $item->title);
		//折扣价
		$itemPrice = _price($item);
		$itemPics[] = "<li><a class=\"pic\" href=\"{$itemUrl}\" target=\"{$target}\" {$item_borderColor}><img src=\"{$itemPic}\" alt=\"{$itemTitle}\"></a> <div class=\"item_con\"><a href='{$itemUrl}' target='{$target}'><span class=\"title\" {$item_titleColor}>{$itemTitle}</span></a><span class=\"price\" {$item_priceColor}><em>RMB:</em>{$itemPrice}</span><div class=\"tag {$p35_tag2}\"><span {$item_tagColor}>{$pr36[$tagIndex]}</span></div></div></li>";
		$p35_navLis[] = "<li></li>";
	}
	$p35Pic = implode($itemPics);
	$p35li = implode($p35_navLis);
	$item_TJ = <<<EOOT
<div class="item_box" {$item_position}>
			<div class="items J_TWidget" data-widget-type="Carousel" {$Carousel_config}>
				<div class="btn_bar">
					<div class="prev_g">
						<div class="leftBg"></div>
					</div>
					<div class="next_g">
						<div class="rightBg"></div>
					</div>
				</div>
				<div class="content_box">
					<ul class="content">{$p35Pic}</ul>
				</div>
				<div class="nav_box">
					<ul class="nav">{$p35li}</ul>
				</div>
			</div>
		</div>
EOOT;
	$custom = $custom . $item_TJ;
}
//宝贝推荐结束
if ($pr27[6]) {
	$pr38s = $c->strEP($p[38], '|');
	$pr39s = $c->strEP($p[39], '|');
	$userPics = array();
	foreach ($pr38s as $picNum => $pr38) {
		$pics = $c->strEP($pr38, ',');
		$picsArr = array(array($pics[0], "background-image:url('{$pics[0]}')"), sT1($pics[1], "width:{$pics[1]}px"), sT1($pics[2], "height:{$pics[2]}px"), sT1($pics[3], "left:{$pics[3]}px"), sT1($pics[4], "top:{$pics[4]}px"),);
		$p39 = $t->setATag(array($pr39s[$picNum], $target));
		$picStyle = $t->setStyles($picsArr);
		$userPics[] = "{$p39[0]}<div class=\"userPic\" {$picStyle}></div>{$p39[1]}";
	}
	$custom = $custom . implode($userPics);
}
?>
<div class="tb-module tshop-um tshop-um-top_c_banner" <?= $p0 ?>>
	<div class="box1">
		<?= $pLogo ?>
		<div class="hotbox"><?= $hotDot ?></div>
		<?= $custom ?>
	</div>
	<div class="box">
		<?= $p1 ?>
		<?= $p[4] ?>
	</div>
	<div class="notice"><?= $pNotice ?></div>
</div>