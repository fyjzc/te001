<?php
global $c, $srd;
$p = $c->inputIfoArr('p', 13);
$target = 'target="' . $p[1] . '"';
function borderColor($colorValue) {
	$s = new StyleRead();
	$value = trim($colorValue);
	return $s->isColor($value) ? 'border-color:' . $value : NULL;
}

?>
<div class="tb-module tshop-um tshop-um-video">
	<div class="box" <?= echoStyle(array($srd->sMarginBottom($p[0]))) ?>>
		<div class="tit <?= $p[2] ? 'd_none' : '' ?>">
			<span <?= echoStyle(array($p[4], $srd->sColor($p[5]), $srd->sFontSize($p[6]), $p[7], borderColor($p[8]))) ?>><?= $p[3] ?></span>
		</div>
		<div class="content">
			<div class="mainUI">
				<div class="video">
					<?php
					$defVideo = 'http://cloud.video.taobao.com/play/u/2667166845/p/1/e/1/t/1/41488128.swf';
					$bPosition = 'background-position: bottom left';
					if ($srd->isImage($p[9])) { ?>
						<div class="img">
							<div class="ib" <?= echoStyle(array($srd->sBgImage($p[9]), $bPosition)) ?>>
								<a href="<?= $srd->isUrl($p[10]) ? $p[10] : '#' ?>" <?= $target ?>></a>
							</div>
						</div>
					<? } else { ?>
						<embed src="<?= $p[9] ? $p[9] : $defVideo ?>"
							   quality="high" width="100%" height="100%" align="middle"
							   type="application/x-shockwave-flash" allowscriptaccess="never"
							   flashvars="scene=taobao_shop"></embed>
					<? } ?>
				</div>
				<div class="infBox">
					<span class="imgPic" <?= echoStyle(array($srd->sBgImage($p[12]))) ?>><i></i></span>
					<span class="weiBoName">WeiBo：<?= $p[11] ?></span>
					<span class="introduce">
						<?php
						function unHtml($content) {                             //定义自定义函数的名称
							$content = htmlspecialchars($content);               //转换文本中的特殊字符
							$content = str_ireplace(chr(13), "<br>", $content);  //替换文本中的换行符
							$content = str_ireplace(chr(32), " ", $content);     //替换文本中的空格
							$content = str_ireplace("[_[", "<", $content);       //替换文本中的小于号
							$content = str_ireplace(")_)", ">", $content);       //替换文本中的大于号
							$content = str_ireplace("|_|", " ", $content);       //替换文本中的空格
							return trim($content);                              //删除文本中首尾的空格
						}

						?>
						<?= unHtml($p[13]) ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>