<?php

class M_item
{
	public $id = 0,$price = 0,$oldPrice = 0,$title = '',$pic = '',$soldCount = 0,$collectedCount = 0,$detailURI = '';

	function getItems($mode,$sortType,$count,array $itemIds = array(),$categoryJson = '',$keyWord = '') {
		global $itemManager;
		$items = $itemManager->queryByKeyword('',$sortType,$count);
		switch ($mode) {
			case 1:
				$items = $itemManager->queryByIds($itemIds,$sortType);
				break;
			case 2:
				$categoryIds = array();
				$itemsTs = array();
				$itemsAll = array();
				$itemIds = array();
				$items = array();
				if ($categoryJson) {
					$categoryArr = json_decode($categoryJson,TRUE);
					foreach ($categoryArr as $ids) {
						if ($ids['childIds'] == '') {
							$categoryIds[] = $ids['rid'];
						} else {
							$childIds = explode(',',$ids['childIds']);
							foreach ($childIds as $childId) {
								$categoryIds[] = $childId;
							}
						}
					}
					foreach ($categoryIds as $categoryId) {
						$itemsTs[] = $itemManager->queryByCategory($categoryId,$sortType,20);
					}
				}
				if (count($itemsTs) > 0) {
					foreach ($itemsTs as $itemsT) {
						foreach ($itemsT as $item) {
							$itemsAll[] = $item;
						}
					}
				}
				if (count($itemsAll) > 0) {
					foreach ($itemsAll as $item) {
						$itemIds[] = $item->id;
					}
					$itemIds_tmp = array_flip(array_flip($itemIds));
					$itemsQs = $itemManager->queryByIds($itemIds_tmp,$sortType);
					for ($i = 0;$i < $count;$i++) {
						$items[$i] = $itemsQs[$i];
					}
				}
				break;
			case 3:
				$items = $itemManager->queryByKeyword($keyWord,$sortType,$count);
				break;
		}
		return $items;
	}

	function itemData($item,$tNum,$picSize) {
		global $uriManager;
		if (isset($item)) {
			$this->id = $item->id;
			$this->price = $this->getRealPrice($item,$tNum);
			$this->oldPrice = $item->price;
			$this->title = $item->title;
			$this->pic = $item->getPicUrl($picSize);
			$this->soldCount = $item->soldCount;
			$this->collectedCount = $item->collectedCount;
			$this->detailURI = $uriManager->detailURI($item);
		} else {
			$this->def_itemData();
		}
	}

	function def_itemData() {
		$this->price = '0.00';
		$this->oldPrice = 0;
		$this->title = 'û��ѡ�񱦱�,��ѡ�񱦱�';
		$this->pic = 'https://img.alicdn.com/imgextra/i1/2667166845/TB2F3ZYkXXXXXaZXXXXXXXXXXXX_!!2667166845.png';
		$this->soldCount = 0;
		$this->collectedCount = 0;
		$this->detailURI = '#';
	}

	function getRealPrice($item,$tNum = 2) {
		$realPrice = $item->price;
		if ($item->discountPrice && $item->discountPrice != $item->price && $item->discountPrice != 0) {
			$realPrice = $item->discountPrice;
		}
		$realPrice = number_format($realPrice,$tNum,'.','');
		return $realPrice;
	}
}

?>
<?php
global $c,$srd;
$p = $c->inputIfoArr('p',20);
$target = 'target="' . $p[1] . '"';
function borderColor($colorValue) {
	$s = new StyleRead();
	$value = trim($colorValue);
	return $s->isColor($value) ? 'border-color:' . $value : NULL;
}

function fontFM($fmVal) {
	return 'font-family:\'' . $fmVal . '\'';
}

?>
<div class="tb-module tshop-um tshop-um-popTop" <?= echoStyle(array($srd->sMarginBottom($p[0]))) ?>>
	<div class="box">
		<div class="h-hd <?= $p[3] ? 'd_none' : '' ?>">
			<div class="h-bg">
				<span class="hd-txt"><?= $p[4] ?></span>
			</div>
		</div>
		<?php
		$picSize = 170;
		$center = 'background-position:center center';
		$sd_n = 'style="display:none"';
		$pd0 = 'padding-top:0';
		$count = $srd->isInt($p[10]) ? ($p[10] > 8 ? 8 : $p[10]) : 8;
		$mItem = new M_item();
		$items = $mItem->getItems($p[5],$p[9],$count,$c->strEP($p[6],','),$p[7],$p[8]);
		$customPics = $c->strEP($p[12],'|');
		?>
		<div class="s_content" <?= echoStyle(array(borderColor($p[2]))) ?>>
			<div class="J_TWidget" data-widget-type="Accordion"
				 data-widget-config="{'triggerCls':'trigger','panelCls':'panel','triggerType': 'mouse', 'multiple':false}">
				<?php
				for ($i = 0;$i < $count;$i++) {
					$s = isset($items[$i]) ? $i : 0;
					$mItem->itemData($items[$s],$p[19],$picSize);
					$itemPic = $customPics[$i] ? $customPics[$i] : $mItem->pic;
					?>
					<div
						class="trigger <?= $i == 0 ? 'ks-active' : '' ?>" <?= echoStyle(array($i == 0 ? $pd0 : NULL)) ?>>
						<i></i>
						<? if ($c->isInChecks(1,$p[13])) { ?>
							<span class="title" title="<?= $mItem->title ?>" <?= echoStyle(array(
								$srd->sColor($p[14]),
								fontFM($p[15]),
							)) ?>><?= $mItem->title ?></span>
						<? } ?>
					</div>
					<div class="panel" <?= $i == 0 ? '' : $sd_n ?>>
						<div class="hide">
							<a href="<?= $mItem->detailURI ?>" <?= $target ?> class="ms_hover" <?= echoStyle(array(
								$srd->sBgImageO($itemPic),
								$center,
							)) ?>>
								<p class="tmc_c"></p>
								<? if (!$p[11]) { ?>
									<div class="p_size">
										<div class="mask"></div>
										<em><?= $i + 1 ?>.&nbsp;ͼƬ��С:</em>
										<span><?= $picSize . 'x' . $picSize ?></span>
									</div>
								<? } ?>
							</a>
						</div>
						<? if ($c->isInChecks(2,$p[13])) { ?>
							<div class="cen">
							<span class="price">
								<em class="d_price" style="font-family: Arial">&yen;</em>
								<em class="p_price" <?= echoStyle(array(
									$srd->sColor($p[16]),
									$srd->sFontSize($p[17]),
									fontFM($p[18]),
								)) ?>><?= $mItem->price ?></em>
							</span>
							</div>
						<? } ?>
					</div>
				<? } ?>
			</div>
		</div>
	</div>
</div>
