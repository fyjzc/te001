<?php
//图片尺寸
function getSizes() {
	$tbSizes = array(
		16,20,24,30,32,36,40,48,50,60,64,70,72,80,88,90,100,110,120,125,128,130,145,160,170,180,190,200,210,220,230,234,
		240,250,270,290,300,310,315,320,336,350,360,400,430,460,468,480,490,540,560,570,580,600,640,670,720,728,760,960,970,
	);
	return $tbSizes;
}

//自定义尺寸处理
function getImg($img) {
	$arraySize = getSizes();
	$newArray = array();
	foreach ($arraySize as $size) {
		if ($size >= $img) {
			$newArray[] = $size;
		}
	}
	$itemPic = $img > 970 ? "970" : min($newArray);
	return $itemPic;
}

//折扣价
function discountPrice($item) {
	if ($item->discountPrice || $item->discountPrice != $item->price) {
		$discountPrice = number_format($item->discountPrice,3,".","");//格式化数字（取三位小数）
	} else {
		$discountPrice = number_format($item->price,3,".","");//同上
	}
	$discountPrice = substr($discountPrice,0,-1);//截取两位小数
	return $discountPrice;
}

//店铺商品相关
function getItems($ids,$categoryId,$keyword,$sortType = " ",$num = "20",$itemData = "1",$idsType = "itemForm") {
	global $itemManager,$uriManager,$rateManager;
	$arraySize = getSizes();
	$itemsObj = array();
	$ratesObj = array();
	if ($ids != NULL && $itemData == 2) {//手动
		if ($idsType == 'itemRateForm') {//评价选择器
			$itemRates = $rateManager->parse($ids);
			foreach ($itemRates->keySet() as $id) {
				$item = $itemManager->queryById($id);
				$rate = $itemRates->get($id);
				$itemsObj[] = $item;
				$ratesObj[] = $rate;
			}
		} else {//宝贝选择器
			$arrayIds = is_string($ids) ? explode(',',$ids) : (array)$ids;
			if ($sortType == " ") {
				foreach ($arrayIds as $id) {
					$item = $itemManager->queryById($id);
					$itemsObj[] = $item;
				}
			} else {
				$itemsObj = $itemManager->queryByIds($arrayIds,$sortType);
			}
		}
	} elseif ($categoryId != NULL && $itemData == 3) {//类目
		$jsonArray = json_decode($categoryId);
		foreach ($jsonArray as $jsonObject) {
			$childIds = explode(",",$jsonObject->childIds);
			if ($childIds != NULL) {
				foreach ($childIds as $childId) {
					$items_xfl = $itemManager->queryByCategory($childId,$sortType,$num);
					foreach ($items_xfl as $itemCate) {
						$itemsObj[] = $itemCate != NULL ? $itemCate : "";
					}
				}
			} else {
				$items_dfl = $itemManager->queryByCategory($jsonObject->rid,$sortType,$num);
				foreach ($items_dfl as $itemCate) {
					$itemsObj[] = $itemCate != NULL ? $itemCate : "";
				}
			}
		}
	} elseif ($keyword != NULL && $itemData == 4 || $itemData == 1) {//关键字||自动
		$kw = $itemData == 1 ? " " : $keyword;
		$kwObj = $itemManager->queryByKeyword($kw,$sortType,$num);
		$newType = str_replace('__','','_' . $sortType);
		$newObj = $sortType == " " ? $kwObj : array_reverse($itemManager->queryByKeyword($kw,$newType,$num));
		$delObj = array_unique(array_merge($kwObj,$newObj));
		$itemsObj = array_merge($delObj,$delObj,$delObj,$delObj,$delObj);
	}
	$skuLists = $itemsObj != NULL ? $itemManager->queryOpenSkuDOListByOpenItemDOList($itemsObj) : "";
	$itemData = $itemsObj != NULL ? $itemData : "5";
	$items = array();
	$qzz = array();
	if ($itemData != 5 || $itemsObj != NULL) {
		foreach ($itemsObj as $key => $item) {
			if ($key < $num) {
				if ($item->exist) {
					$qzz['url'] = $uriManager->detailURI($item);
					foreach ($arraySize as $imgSize) {
						$qzz['pic' . $imgSize] = $item->getPicUrl($imgSize);
					}
					$qzz['title'] = $item->title;
					$qzz['price'] = $item->price;
					$qzz['discountPrice'] = discountPrice($item);
					$qzz['zhekou'] = number_format(str_replace(',','',discountPrice($item)) / str_replace(',','',$item->price),2) * 10;
					$qzz['soldCount'] = $item->soldCount;
					$qzz['collectedCount'] = $item->collectedCount;
					$qzz['id'] = $item->id;
					$qzz['skuList'] = $skuLists[$key];
					foreach ($arraySize as $imgSize) {
						$qzz['skuPics' . $imgSize] = $itemManager->getSkuPropertyPics($skuLists[$key],$imgSize,$imgSize);
					}
					$qzz['rateList'] = $ratesObj[$key] != NULL ? $ratesObj[$key] : "";
				}
			} else {
				break;
			}
			$items[] = $qzz;
		}
	}
	//兼容2处理,只有宝贝数量为0的做处理 count($items) < $num 改为 count($items) < 1
	if ($itemData == 5 || (count($items) < 1 && $itemData != 2)) {//风格预览||兼容处理
		$aa = $num - count($items);
		$imgUrl = array(
			"http://img02.taobaocdn.com/imgextra/i2/34251258/TB2Dc8mcVXXXXbHXpXXXXXXXXXX_!!34251258.jpg",
			"https://img.alicdn.com/imgextra/i1/2667166845/TB2F3ZYkXXXXXaZXXXXXXXXXXXX_!!2667166845.png",
		);//定义图片数组（根据需求，需要多少图，输入淘宝图片空间的图链接即可）
		for ($i = 0;$i < $aa;$i++) {
			$mtRand = mt_rand(0,(count($imgUrl) - 1));
			$qzz['url'] = "http://www.taobao.com/";
			foreach ($arraySize as $imgSize) {
				$qzz['pic' . $imgSize] = $imgUrl[$mtRand] . "_" . $imgSize . "x" . $imgSize . ".jpg";
			}
			$qzz['title'] = "宝贝数据未添加，请点击右上角的编辑按钮选择添加您的宝贝";
			$qzz['price'] = "598.00";
			$qzz['discountPrice'] = "398.98";
			$qzz['zhekou'] = number_format(398.98 / 598.00,2) * 10;
			$qzz['soldCount'] = "1588";
			$qzz['collectedCount'] = "988";
			$qzz['id'] = "1" . $i;
			$items[] = $qzz;
		}
	}
	return $items;
}

?>
<?php
function getMod($m = 'm',$in = 20) {
	global $_MODULE;
	$p = array();
	for ($i = 0;$i <= $in;$i++) {
		$p[$i] = $_MODULE[$m . $i];
	}
	return $p;
}

function isInt($v) {
	return intval($v) . '' === $v ? 1 : 0;
}

function isColor($v) {
	preg_match_all('/#/',$v,$count);
	$strLen = strlen($v);
	$value = str_replace('#','',$v);
	if (is_numeric(hexdec($value)) && $v[0] == '#' && count($count[0]) == 1 && ($strLen == 4 || $strLen == 7)) {
		return 1;
	}
	return 0;
}

function isUrl($v) {
	return ((strpos($v,'http://') === 0) || (strpos($v,'https://') === 0)) ? 1 : 0;
}

function isImage($v) {
	return isUrl($v) && (strpos($v,'.png') || strpos($v,'.jpg') || strpos($v,'.gif')) ? 1 : 0;
}

function sStyle($v,$dl = '|') {
	$tr = explode($dl,$v);
	return $v ? ' style="' . implode(';',$tr) . '"' : '';
}

function sMb($v) {
	return isInt($v) ? 'margin-bottom:' . $v . 'px|' : '';
}

function sBgIO($v) {
	return 'background:url(https:' . $v . ') center center no-repeat|';
}

function sColor($v) {
	return isColor($v) ? 'color:' . $v . '|' : '';
}

function sFontFamily($v) {
	return 'font-family:\'' . $v . '\'|';
}

function sFontSize($v) {
	return isInt($v) ? 'font-size:' . $v . 'px|' : '';
}

function sBgC($v) {
	return isColor($v) ? 'background-color:' . $v . '|' : '';
}

?>
<?php
$m = getMod('m',18);
?>
<div class="tb-module tshop-um tshop-um-itemTop" <?= sStyle(sMb($m[0])) ?>>
	<div class="box">
		<div class="h-hd <?= $m[2] ? '' : 'd_none' ?>">
			<div class="h-bg">
				<span class="hd-txt"><?= $m[3] ?></span>
			</div>
			<div class="h_bd">
				<div class="J_TWidget mainbox" data-widget-type="Tabs"
					 data-widget-config="{'effect':'none', 'autoplay': <?= $m[11] == 1 ? 'true' : 'false' ?>,'navCls': 'h_nav','contentCls': 'h_con','triggerType': '<?= $m[12] == 1 ? 'mouse' : 'click' ?>','activeTriggerCls':'active'}">
					<div class="h_navBox">
						<ul class="h_nav">
							<? for ($h = 0;$h < 2;$h++) { ?>
								<? if ($m[10] == 1) {
									$active = $h ? '' : 'active';
								} else {
									$active = $h ? 'active' : '';
								} ?>
								<li class="h_tit <?= $active ?>"><span><?= $h ? '收藏数' : '销售量' ?></span></li>
							<? } ?>
						</ul>
					</div>
					<div class="h_conBox">
						<ul class="h_con">
							<?php
							$count = isInt($m[4]) ? ($m[4] > 10 ? 10 : $m[4]) : 10;
							for ($h = 0;$h < 2;$h++) {
								$liSl = '';
								if ($m[10] == 1) {
									$liSl = $h ? 'display: none|' : '';
								} else {
									$liSl = $h ? '' : 'display: none|';
								}
								$items = getItems($m[7],$m[8],$m[9],$h ? '_HotKeep' : '_hotsell',$count,$m[6]);
								?>
								<li class="h_conInf" <?= sStyle($liSl) ?>>
									<?php
									for ($i = 0;$i < $count;$i++) {
										$price = number_format($items[$i]['discountPrice'],$m[18],'.','');
										if (!isset($items[$i])) {
											break;
										} ?>
										<div class="itemBox">
											<div class="imgBox" <?= sStyle(sBgIO($items[$i]['pic' . getImg(50)])) ?>>
												<a <?= $m[1] ?> href="<?= $items[$i]['url'] ?>"></a>
												<div class="bGImg"
													<?= sStyle(sBgIO($items[$i]['pic' . getImg(100)])) ?>>
													<a <?= $m[1] ?> href="<?= $items[$i]['url'] ?>"></a>
												</div>
												<div class="bGc"></div>
											</div>
											<div class="txtBox">
												<div
													<?= sStyle(sColor($m[13]) . sFontFamily($m[14])) ?>
													class="tit">
													<?= $items[$i]['title'] ?></div>
												<div class="price">
													<span class="symbol" style="font-family: Arial">&yen;</span>
													<em <?= sStyle(sColor($m[15]) . sFontSize($m[16]) .
														sFontFamily($m[17])) ?>><?= $price ?></em>
												</div>
												<div class="sold">
													<span><?= $h ? '已收藏' : '已售出'; ?></span>
													<em><?= $items[$i][$h ? 'collectedCount' : 'soldCount'] ?></em>
													<span><?= $h ? '笔' : '件'; ?></span>
												</div>
											</div>
										</div>
									<? } ?>
								</li>
							<? } ?>
						</ul>
						<div class="gd">
							<a <?= $m[1] ?> href="<?= isUrl($m[5]) ? $m[5] : $uriManager->searchURI() ?>">查看更多宝贝</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
