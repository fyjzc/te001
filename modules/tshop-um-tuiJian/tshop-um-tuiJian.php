<?php
//获取XML数据
function getMod($m = 'm',$in = 20) {
	global $_MODULE;
	$p = array();
	for ($i = 0;$i <= $in;$i++) {
		$p[$i] = $_MODULE[$m . $i];
	}
	return $p;
}

function isInt($v) {
	return intval($v) . '' == $v ? 1 : 0;
}

function isColor($v) {
	preg_match_all('/#/',$v,$count);
	$strLen = strlen($v);
	$value = str_replace('#','',$v);
	if (is_numeric(hexdec($value)) && $v[0] == '#' && count($count[0]) == 1 && ($strLen == 4 || $strLen == 7)) {
		return 1;
	}
	return 0;
}

function isUrl($v) {
	return ((strpos($v,'http://') === 0) || (strpos($v,'https://') === 0)) ? 1 : 0;
}

function isImage($v) {
	return isUrl($v) && (strpos($v,'.png') || strpos($v,'.jpg') || strpos($v,'.gif')) ? 1 : 0;
}

//dom样式
function sStyle($v,$dl = '|') {
	$tr = explode($dl,$v);
	return $v ? ' style="' . implode(';',$tr) . '"' : '';
}

function sFontFamily($v) {
	return 'font-family:\'' . $v . '\'|';
}

function sMb($v) {
	return isInt($v) ? 'margin-bottom:' . $v . 'px|' : '';
}

function sFontSize($v) {
	return isInt($v) ? 'font-size:' . $v . 'px|' : '';
}

function sBgI($v) {
	return isImage($v) ? 'background:url(' . $v . ') center center no-repeat|' : '';
}

function sBDColor($v) {
	return isColor($v) ? 'border-color:' . $v . '|' : '';
}

$fms = array('微软雅黑','黑体','宋体','楷体','华文行楷','隶书','幼圆','impact','Arial');
$m = getMod('m',10);
$qUrl = isUrl($m[5]) ? $m[5] : $uriManager->searchURI();
?>
<div class="tb-module tshop-um tshop-um-tuiJian" <?= sStyle(sMb($m[0])) ?>>
	<div class="<?= $m[2] ? 'bd_none ' : '' ?>box" <?= sStyle(sFontFamily($fms[0]) . sBDColor($m[3])) ?>>
		<div class="bgi" <?= sStyle(sBgI($m[4])) ?>>
			<? if (isImage($m[4])) { ?>
				<a href="<?= $qUrl ?>" <?= $m[1] ?>></a>
			<? } ?>
		</div>
		<? if (!isImage($m[4])) { ?>
			<div class="left">
				<span class="txt1"><?= $m[7] ?></span>
				<a href="<?= $qUrl ?>" <?= $m[1] ?> <?= sStyle(sFontFamily($fms[0])) ?>>点击领取→</a>
			</div>
			<div class="right">
				<div class="text">
					<div class="txt2"><?= $m[8] ?></div>
					<div class="txt3"><?= $m[9] ?></div>
					<div class="price" <?= sStyle(sFontFamily($fms[8])) ?>>
						<? if (strlen($m[6]) == 1) { ?>
							<span class="yi"><?= $m[6] ?></span>
							<span class="d">.00</span>
						<? } elseif (strlen($m[6]) == 2) { ?>
							<span class="yi"><?= $m[6] ?></span>
							<span class="d" <?= sStyle(sFontFamily($fms[0]) . sFontSize(16) . 'left:-10px|') ?>>元</span>
						<? } else {
							echo $m[6];
						} ?>
					</div>
				</div>
				<a href="<?= $qUrl ?>" <?= $m[1] ?>></a>
			</div>
		<? } ?>
	</div>
</div>