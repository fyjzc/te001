<?php

class M_item
{
	public $id = 0, $price = 0, $oldPrice = 0, $title = '', $pic = '', $soldCount = 0, $collectedCount = 0, $detailURI = '';

	function getItems($mode, $sortType, $count, array $itemIds = array(), $categoryJson = '', $keyWord = '') {
		global $itemManager;
		$items = $itemManager->queryByKeyword('', $sortType, $count);
		switch ($mode) {
			case 1:
				$items = $itemManager->queryByIds($itemIds, $sortType);
				break;
			case 2:
				$categoryIds = array();
				$itemsTs = array();
				$itemsAll = array();
				$itemIds = array();
				$items = array();
				if ($categoryJson) {
					$categoryArr = json_decode($categoryJson, TRUE);
					foreach ($categoryArr as $ids) {
						if ($ids['childIds'] == '') {
							$categoryIds[] = $ids['rid'];
						} else {
							$childIds = explode(',', $ids['childIds']);
							foreach ($childIds as $childId) {
								$categoryIds[] = $childId;
							}
						}
					}
					foreach ($categoryIds as $categoryId) {
						$itemsTs[] = $itemManager->queryByCategory($categoryId, $sortType, 20);
					}
				}
				if (count($itemsTs) > 0) {
					foreach ($itemsTs as $itemsT) {
						foreach ($itemsT as $item) {
							$itemsAll[] = $item;
						}
					}
				}
				if (count($itemsAll) > 0) {
					foreach ($itemsAll as $item) {
						$itemIds[] = $item->id;
					}
					$itemIds_tmp = array_flip(array_flip($itemIds));
					$itemsQs = $itemManager->queryByIds($itemIds_tmp, $sortType);
					for ($i = 0; $i < $count; $i++) {
						$items[$i] = $itemsQs[$i];
					}
				}
				break;
			case 3:
				$items = $itemManager->queryByKeyword($keyWord, $sortType, $count);
				break;
		}
		return $items;
	}

	/**
	 * @param $item
	 * @param $tNum
	 * 小数点位数
	 * @param $picSize
	 */
	function itemData($item, $tNum, $picSize) {
		global $uriManager;
		if (isset($item)) {
			$this->id = $item->id;
			$this->price = $this->getRealPrice($item, $tNum);
			$this->oldPrice = $item->price;
			$this->title = $item->title;
			$this->pic = $item->getPicUrl($picSize);
			$this->soldCount = $item->soldCount;
			$this->collectedCount = $item->collectedCount;
			$this->detailURI = $uriManager->detailURI($item);
		} else {
			$this->def_itemData();
		}
	}

	function def_itemData() {
		$this->price = '0.00';
		$this->oldPrice = 0;
		$this->title = '没有选择宝贝,请选择宝贝';
		$this->pic = 'https://img.alicdn.com/imgextra/i1/2667166845/TB2F3ZYkXXXXXaZXXXXXXXXXXXX_!!2667166845.png';
		$this->soldCount = 0;
		$this->collectedCount = 0;
		$this->detailURI = '#';
	}

	function getRealPrice($item, $tNum = 2) {
		$realPrice = $item->price;
		if ($item->discountPrice && $item->discountPrice != $item->price && $item->discountPrice != 0) {
			$realPrice = $item->discountPrice;
		}
		$realPrice = number_format($realPrice, $tNum, '.', '');
		return $realPrice;
	}
}

?>
<?php
global $c, $t, $srd, $uriManager;
$p = $c->inputIfoArr('p', 50);
$tp = $c->inputIfoArr('tp', 10);
$m_item = new M_item();
$sortType = $p[16];
$count = $srd->isInt($p[17]) ? $p[17] : 12;
$items = $m_item->getItems($p[12], $sortType, $count, explode(',', $p[13]), $p[14], $p[15]);
$center = 'background-position:center center';
$target = 'target="' . $p[1] . '"';
$font_m = 'font-family:\'微软雅黑\'';
$font_s = 'font-family:\'宋体\'';
$font_a = 'font-family:arial';
$d_none = 'display:none';
$margin_modules = $c->strEP($p[0], '|');
$saColor = $c->strEP($p[3], '|');
$borderColor = $saColor[1] ? $saColor[1] : NULL;
$moduleFm = 'font-family:\'' . $p[4] . '\'';
$bdcStr = 'border-color:';
?>
<div class="tb-module tshop-um tshop-um-hotSell">
	<div class="box <?= $tp[0] ? '' : 'border' ?>" <?= echoStyle(
		array($srd->sMarginTop($margin_modules[0]),
			$srd->sMarginBottom($margin_modules[1]),
			$srd->sColor($p[2]),
			$srd->sBgColor($saColor[0]),
			$borderColor,
			$moduleFm,
			$srd->sColor($p[5]),
		)) ?>>
		<div class="header">
			<div class="hd_bg" <?= echoStyle(array($srd->sBgImage($tp[1]))) ?>>
				<? if (!$srd->isImage($tp[1])) { ?>
					<span class="hd_txt" <?= echoStyle(array($font_m,)) ?>><?= $p[21] ?></span> <a class="more_btn"
																								   href="<?= $srd->isUrl($p[23]) ? $p[23] : $uriManager->searchURI(); ?>" <?= $target ?> <?= echoStyle(array($font_s, $p[22] ? $d_none : NULL,)) ?>>查看更多
																																																													>></a>
				<? } elseif ($srd->isUrl($tp[2])) { ?>
					<a <?= $target ?> href="<?= $tp[2] ?>"></a>
				<? } ?>
			</div>
		</div>
		<div class="i_bd">
			<?php
			$bgiS = $c->strEP($p[20], '|');
			?>
			<ul>
				<? for ($i = 0; $i < $count; $i++) {
					$picSize = '460';
					$pd_left = '30px';
					$mod = 2;
					$bPadding = 'padding: 20px 0 20px 22px';
					$boxWidth = '441';
					$ifBoxWidth = '371';
					$ifBoxWidth1 = '390';
					switch ($p[18]) {
						case 3:
							$picSize = '300';
							$pd_left = '25px';
							$mod = 3;
							$bPadding = 'padding: 18px 0 18px 12px';
							$boxWidth = '300';
							$ifBoxWidth = '230';
							$ifBoxWidth1 = '230';
							break;
						case 4:
							$picSize = '230';
							$pd_left = '10px';
							$mod = 4;
							$bPadding = 'padding: 18px 0 18px 14px';
							$boxWidth = '220';
							$ifBoxWidth = '149';
							$ifBoxWidth1 = '160';
							break;
					}
					$padding = 'padding:20px 0 20px ' . ($i % $mod == 0 ? '0' : $pd_left);
					if ($tp[0]) {
						$boxWidth = $picSize;
						$ifBoxWidth = $ifBoxWidth1;
					}
					$padding = $tp[0] ? $padding : $bPadding;
					$s = isset($items[$i]) ? $i : 0;
					$m_item->itemData($items[$s], $p[27], $picSize);
					$bgi = $srd->isImage($bgiS[$i]) ? $bgiS[$i] : $m_item->pic;
					?>
					<li class="item_box" <?= echoStyle(array($padding, $srd->sWidth($boxWidth),)) ?>>
						<div class="img_box" <?= echoStyle(array($srd->sHeight($boxWidth),)) ?>>
							<a <?= $target ?> href="<?= $m_item->detailURI; ?>"
											  class="imgInfo" <?= echoStyle(array($srd->sBgImageO($bgi), $center,)) ?>></a>
							<div class="picSize_info" <?= echoStyle(array($p[19] ? $d_none : NULL,)) ?>><?= $i + 1 ?>
								图片尺寸：<?= $boxWidth . '*' . $boxWidth ?>
							</div>
						</div>
						<div
							class="msg_box <?= $p[24] ? 'd_none' : '' ?>" <?= echoStyle(array($srd->sBgColor($p[6]))) ?>>
							<div class="info_box" <?= echoStyle(array($srd->sWidth($ifBoxWidth),)) ?>>
								<div
									class="price_box" <?= echoStyle(array($srd->isColor($p[10]) ? $bdcStr . $p[10] : NULL,)) ?>>
									<span class="sold_on" <?= echoStyle(array(!$p[29] ? $d_none : NULL,)) ?>>
										<span
											class="h_collected <?= $p[29] == 1 ? 'd_none' : '' ?>">收藏:<b <?= echoStyle(array($srd->sColor($p[9]))) ?>><?= $m_item->collectedCount ?></b><span
												class="data_unit_on <?= $p[30] ? 'd_none' : '' ?>">人</span></span>
										<span
											class="h_sold <?= $p[29] == 2 ? 'd_none' : '' ?>">已售:<b <?= echoStyle(array($srd->sColor($p[9]))) ?>><?= $m_item->soldCount ?></b><span
												class="data_unit_on <?= $p[30] ? 'd_none' : '' ?>">件</span></span>
									</span>
									<span class="h-price">
										<?php
										if ($p[25] == '￥') {
											$p[25] = '&yen;';
										}
										?>
										<span
											class="h_price_symbol"<?= echoStyle(array($font_a)) ?>><?= $p[25] ?></span>
										<em <?= echoStyle(array($srd->sColor($p[8]), $srd->sFontSize($p[26]))) ?>><?= $m_item->price ?></em>
									</span>
								</div>
								<a class="h_desc" href="<?= $m_item->detailURI ?>"
								   title="<?= $m_item->title ?>" <?= $target ?>>
									<span <?= echoStyle(array($moduleFm, $srd->sColor($p[5]), $srd->sColor($p[7]),)) ?>><?= $m_item->title ?></span>
								</a>
							</div>
							<div
								class="h_dim <?= $p[28] ? 'd_none' : '' ?>" <?= echoStyle(array($srd->isColor($p[11]) ? $bdcStr . $p[11] : NULL,)) ?>>
								<img
									src="http://gqrcode.alicdn.com/img?type=ci&item_id=<?= $m_item->id ?>&v=1&w=60&h=60">
							</div>
						</div>
					</li>
				<? } ?>
			</ul>
		</div>
	</div>
</div>
