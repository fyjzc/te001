<?php
global $c, $t, $srd, $uriManager;
$p = $c->inputIfoArr('m', 50);
?>
<div class="tb-module tshop-um tshop-um-mBar">
	<?php
	$pr1 = $c->strEP($p[1], '|');
	?>
	<div class="box" <?= echoStyle(array(
		$srd->sMarginTop($pr1[0]),
		$srd->sMarginBottom($pr1[1]),
	)) ?>>
		<div class="hd_t1">
			<div class="hd_bg">
				<span class="hd_txt">当季热卖款</span>
				<a href="<?= $uriManager->searchURI() ?>" class="more_btn"
				   target="<?= $p[2] ?>">查看更多 <span>>></span></a>
			</div>
		</div>
	</div>
</div>
