<?php
global $c, $srd, $regionWidth;
$p = $c->inputIfoArr('p', 9);
$target = 'target="' . $p[1] . '"';
$center = 'background-position:center center';
$pic_950_1920 = 'https://gdp.alicdn.com/imgextra/i2/513051429/TB2sNtMtXXXXXb3XXXXXXXXXXXX-513051429.jpg';
$pic_950_950 = 'https://gdp.alicdn.com/imgextra/i3/513051429/TB2HJZLsFXXXXbaXXXXXXXXXXXX-513051429.jpg';
$pic_950_f = 'http://tae-sdk.taobao.com:8080/te001/modules/tshop-um-nbtj/assets/images/bg1.jpg';
$pic_750 = 'https://gdp.alicdn.com/imgextra/i1/513051429/TB2YzLWspXXXXbKXXXXXXXXXXXX-513051429.jpg';
$pic_750_f = 'https://gdp.alicdn.com/imgextra/i4/112394247/TB2CEsIsFXXXXbpXXXXXXXXXXXX-112394247.jpg';
?>
<div class="tb-module tshop-um tshop-um-actional">
	<div class="box box_<?= $regionWidth ?>" <?= echoStyle(array($srd->sMarginBottom($p[0]))) ?>>
		<?php
		$w1920 = NULL;
		$w1920ML = NULL;
		$sHeight = NULL;
		$picUrl = NULL;
		$picLink = NULL;
		$bg_pof = NULL;
		switch ($regionWidth) {
			case 950:
				$sHeight = $p[2];
				$picture = $pic_950_950;
				if ($p[3] == 1 || $p[3] == 3) {
					$w1920 = $srd->sWidth(1920);
					$w1920ML = $srd->sMarginLeft(-485);
					$picture = $pic_950_1920;
				}
				if ($p[3] == 3 || $p[3] == 4) {
					$bg_pof = 'background-attachment: fixed';
					$picture = $pic_750_f;
				}
				if($p[3]==4){
					$picture = $pic_750_f;
				}
				$picUrl = $srd->isImage($p[4]) ? $p[4] : $picture;
				$picLink = $p[5];
				break;
			case 750:
				$sHeight = $p[6];
				$picture = $pic_750;
				if ($p[7] == 2) {
					$bg_pof = 'background-attachment: fixed';
					$picture = $pic_750_f;
				}
				$picUrl = $srd->isImage($p[8]) ? $p[8] : $picture;
				$picLink = $p[9];
				break;
		}
		?>
		<div class="img" <?= echoStyle(array($srd->sHeight($sHeight))) ?>>
			<div class="pic" <?= echoStyle(array(
				$w1920,
				$w1920ML,
				$srd->sBgImage($picUrl),
				$center,
				$bg_pof,
			)) ?>>
				<a href="<?= $picLink ? $picLink : '#' ?>" <?= $target ?>></a>
			</div>
		</div>
		<div class="mask"></div>
	</div>
</div>
