<?php
?>
<?php
$focus_images = array(
	'https://img.alicdn.com/imgextra/i3/2667166845/TB2o63WbhvzQeBjSZFqXXXN5VXa-2667166845.jpg',
	'https://img.alicdn.com/imgextra/i1/2667166845/TB2vHg5bX_AQeBjSZFtXXbFBVXa-2667166845.jpg',
	'https://img.alicdn.com/imgextra/i3/2667166845/TB2wyM4blLzQeBjSZFDXXc5MXXa-2667166845.jpg',
);
$p = $c->inputIfoArr('a', 25);
$p_select = "class=\"selected\"";
$pr0 = $c->strEP($p[0], '|');
$ps0 = $t->setStyle(array($pr0[0] . '0' => "margin-top:{$pr0[0]}px", $pr0[1] . '1' => "margin-bottom:{$pr0[1]}px"));
$pst1 = $c->_r_sm_3($p[1] == '0', '_self', '_blank');
$ps1 = "target=\"{$pst1}\"";
$pr3_def = array(0 => "<a {$ps1} href=\"" . $t->url . "\"><img src=\"{$focus_images[0]}\" alt=\"\"></a>", 1 => "<a {$ps1} href=\"" . $t->url . "\"><img src=\"{$focus_images[1]}\" alt=\"\"></a>", 2 => "<a {$ps1} href=\"" . $t->url . "\"><img src=\"{$focus_images[2]}\" alt=\"\"></a>",);
$pr3 = array();
$pr3_1 = array();
$prt3 = $c->strEP($p[4], '#');
$pr3_url = array();
$ps3_w = null;
$ps3_w_nav = null;
foreach ($prt3 as $k => $v) {
	$pr3t = $c->strEP($v, '|');
	$pr3t[1] = $c->_r_sm_3($pr3t[1], $pr3t[1], '#');
	$pr3_0 = $c->_r_sm($pr3t[0], "<a {$ps1} href=\"{$pr3t[1]}\"><img src=\"{$pr3t[0]}\" alt=\"\"></a>");
	if ($pr3_0) $pr3[$k] = $pr3_0;
	$pr3_urlT = $srd->sUrl($pr3t[1]);
	if ($pr3_urlT) $pr3_url[$k] = $pr3_urlT;
}
$ps3_def = implode($pr3_def);
$pr9 = $c->strEP($p[7], '|');
if ($pr3) {
	for ($i = 0; $i < count($pr3); $i++) {
		$p_select = $c->_r_sm($i == 0, $p_select);
		switch ($p[6]) {
			case 0:
				$pr3_1[$i] = "<li {$p_select}></li>";
				$pr3_wli += 42;
				break;
			case 1:
				$ps5_url = $c->_r_sm_3($pr3_url[$i], $pr3_url[$i], $t->url);
				$pr3_1[$i] = "<li {$p_select}><a {$ps1} href='{$ps5_url}'><span>{$pr9[$i]}</span></a></li>";
				$pr3_wli += 153;
				break;
			case 2:
				$pr3_1[$i] = "<li {$p_select}>{$pr3[$i]}</li>";
				$pr3_wli += 205;
		}
		$pr3_wp = 5;
		$prs3_wn = $pr3_wli + $pr3_wp;
		$ps3_w = "style=\"width:{$prs3_wn}px;\"";
		$ps3_w_nav = "style=\"margin-left:-" . ($prs3_wn / 2) . "px;\"";
	}
} else {
	for ($i = 0; $i < 3; $i++) {
		$p_select = $c->_r_sm($i == 0, $p_select);
		switch ($p[6]) {
			case 0:
				$pr3_1[$i] = "<li {$p_select}></li>";
				break;
			case 1:
				$pr3_1[$i] = "<li {$p_select}><a {$ps1} href=\"" . $t->url . "\"><span>{$pr9[$i]}</span></a></li>";
				break;
			case 2:
				$pr3_1[$i] = "<li {$p_select}>{$pr3_def[$i]}</li>";
		}
	}
}
$pr5 = array(0 => 'ft_nav_rec', 1 => 'ft_nav_title', 2 => 'ft_nav_thumb', 3 => 'ft_hide',);
$ps5 = $pr5[$p[6]];
$ps3 = $pr3 ? implode($pr3) : $ps3_def;
$ps3_li = implode($pr3_1);
if (trim($p[5])) {
	$ps4_1 = "style=\"height:{$p[5]}px;\"";
	$ps4_2 = "style=\"top:" . ($p[5] / 2 - 40) . "px;\"";
}
$pr6 = array(0 => 'btn_def', 1 => 'btn_vs', 2 => 'btn_hide',);
$ps6 = $pr6[$p[8]];
$ps7 = $p[9] == 0 ? 'true' : 'false';
$pr8 = array(0 => 'scrollx', 1 => 'scrolly', 2 => 'fade', 3 => 'none',);
$ps8 = $pr8[$p[10]];
$ps10 = $p[11] == 0 ? 'mouse' : 'click';
$ps11 = $p[12] == 0 ? 'true' : 'false';
$pr12[0] = 0;
for ($i = 0; $i < 7; $i++) {
	$pr12[$i] = $i / 10;
}
$ps12 = $pr12[$p[13]];
$pr13[0] = 0;
$pr13_s = 1.8;
for ($i = 0, $pr13_s = 1.8; $i < 6; $i++) {
	$pr13[$i] = $pr13_s;
	$pr13_s = $pr13_s - 0.3;
}
$ps13 = $pr13[$p[14]];
$pr14[0] = 0;
for ($i = 0; $i < 10; $i++) {
	$pr14[$i] = $i + 1;
}
$ps14 = $pr14[$p[15]];
$pr15 = array(0 => 'backIn', 1 => 'backOut', 2 => 'elasticIn', 3 => 'elasticOut', 4 => 'easeInStrong', 5 => 'easeOutStrong', 6 => 'bounceIn', 7 => 'bounceOut', 8 => 'bounceBoth', 9 => 'easeIn', 10 => 'easeOut', 11 => 'easeBoth', 12 => 'easeBothStrong', 13 => 'easeNone',);
$ps15 = $pr15[$p[16]];
$Carousel_config = "data-widget-config=\"{'effect':'{$ps8}','easing':'{$ps15}','circular':{$ps11},'autoplay':{$ps7},
'contentCls':'content_g','navCls':'ma_g','activeIndex':0,'activeTriggerCls':'selected','triggerType':'{$ps10}',
'duration':{$ps13},'interval':{$ps14},'delay':{$ps12},'prevBtnCls': 'prev_g','nextBtnCls': 'next_g'}\"";
$p17_lis = null;
$p16_hide = $c->_r_sm($p[3] == 0, 'd_hide');
switch ($p[2]) {
	case 0:
		$ps16 = $c->_r_sm_3($p[5], $p[5], '720') . 'px';
		$ps16_s = <<<THINK
        <div class="u_help {$p16_hide}">
            <span>ͼƬ�ߴ�(��):1920px*$ps16</span>
            <span>��ǰģ��:��ҳ��ͼ�ֲ�</span>
        </div>
THINK;
		$ps2 = <<<THINK
<div class="lb {$ps6}">
<div {$ps4_1}class="J_TWidget" data-widget-type="Carousel" {$Carousel_config}>
<div class="btn_bar">
<div class="prev_g" {$ps4_2}></div>
<div class="next_g" {$ps4_2}></div></div>
<div class="content_g">{$ps3}</div>
<div class="ft_nav {$ps5}" {$ps3_w_nav}>
<div class="ft_bg" {$ps3_w}></div>
<ul class="ma_g">{$ps3_li}</ul>
</div></div></div>
THINK;
		break;
	case 1:
		$ps16_s = <<<THINK
        <div class="$p16_hide u_help ">
            <span>����ͼƬ�ߴ�:360px*360px</span>
            <span>��ǰģ��:��ҳ��ͼ�ֲ�</span>
        </div>
THINK;
		$p19 = $p[19] == 0 ? 'true' : 'false';
		$Carousel_config = "data-widget-config=\"{'effect':'fade','easing':'backOut','autoplay':{$p19},
        'contentCls':'content_g','navCls':'ma_g','activeIndex':0,'activeTriggerCls':'selected','duration':0.8,'interval':4,
        'disableBtnCls':'disable','prevBtnCls':'prev_g','nextBtnCls':'next_g','delay':0.1,'triggerType':'mouse'}\"";
		$pr17 = $itemManager->queryByKeyword('', 'hotsell', 5);
		if ($p[17]) {
			$pr17 = $itemManager->queryByIds($c->strEP($p[17], ','), 'hotsell');
		}
		$pr17_li = array();
		$pr17_items = array();
		for ($ni = 0; $ni < 5; $ni++) {
			for ($a = 0; $a < 5; $a++) {
				$num_li = ($a + $ni) == 0 ? 0 : ($a + $ni) % 5;
				if ($pr17[$num_li]->exist) {
					$pr17_ifo = array(
						$pr17[$num_li]->id,
						$uriManager->detailURI($pr17[$num_li]),
						$pr17[$num_li]->getPicUrl(360),
						$pr17[$num_li]->discountPrice ? $pr17[$num_li]->discountPrice : $pr17[$num_li]->price,
						$pr17[$num_li]->title,
					);
				}
				$p17_id = $pr17_ifo[0] ? $pr17_ifo[0] : '0';
				$p17_detailURI = $pr17_ifo[1] ? $pr17_ifo[1] : "https://www.taobao.com";
				$p17_PicUrl = $pr17_ifo[2] ? $pr17_ifo[2] : "https://img.alicdn.com/imgextra/i1/2667166845/TB2F3ZYkXXXXXaZXXXXXXXXXXXX_!!2667166845.png";
				$p17_p = $pr17_ifo[3] ? $pr17_ifo[3] : '0.00';
				$p17_price = sprintf('%.2f', $p17_p);
				$p17_price = number_format($p17_p, 2, '.', '');
				$p17_title = $pr17_ifo[4] ? $pr17_ifo[4] : '��ѡ�񱦱�!';
				$items_str = <<<THINK
<div class="images">
    <a {$ps1} href="{$p17_detailURI}">
        <img src="{$p17_PicUrl}"></a>
    <div class="dot"></div>
</div>
<div class="price">
    <span class="n_prc">RMB:&nbsp;&nbsp;{$p17_price}</span>
    <span class="old_price" style="display:none;">
        ԭ��: <b>��0.00</b>
    </span>
</div>
<div class="tools">
    <div
        class="share sns-widget"
        data-sharebtn='{type:"item",key:"{$p17_id}",title:"",comment:"",pic:"",client_id:68,skinType:2,isShowFriend:false}'>
        <div class="hv"></div>
        <div class="text">����</div>
    </div>
    <div class="fav">
        <a target="_blank" href="https://favorite.taobao.com/popup/add_collection.htm?itemtype=1&id={$p17_id}">
            <div class="hv"></div>
        </a>
        <div class="text">�ղ�</div>
    </div>
    <div class="code">
        <div class="hv"></div>
        <div class="code_img" style="background:url(https://gqrcode.alicdn.com/img?type=ci&item_id={$p17_id}&v=1&w=90&h=90)"></div>
        <div class="text">��ά��</div>
    </div>
    <div class="buy_car">
        <a class="J_CartPluginTrigger" href="{$p17_detailURI}">
        <div class="hv"></div></a>
        <div class="text">���ﳵ</div>
    </div>
</div>
<div class="title">
    <a {$ps1} href="{$p17_detailURI}">{$p17_title}</a>
</div>
THINK;
				$nmi = $a + 1;
				$pr17_items[$a] = "<div class=\"item i{$nmi}\">{$items_str}</div>";
			}
			$pr17_divs = implode($pr17_items);
			$pr17_dp = $ni == 0 ? 'block' : 'none';
			$pr17_li[$ni] = "<li class=\"items\" style=\"display:{$pr17_dp};\">{$pr17_divs}</li>";
		}
		$p17_lis = implode($pr17_li);
		$p18 = $c->_r_sm($p[18], "style=\" background-image:url({$p[18]});\"");
		$ps2 = <<<THINK
<div class="goods_box">
            <div class="bg_box" {$p18}></div>
            <div {$ps4_1} class="J_TWidget" data-widget-type="Carousel" {$Carousel_config}>
                <div class="content_box">
                    <ul class="content_g">{$p17_lis}</ul>
                </div>
                <div class="btn_bar">
                    <div class="prev_g" {$ps4_2}></div>
                    <div class="next_g" {$ps4_2}></div>
                </div>
                <div class="ft_nav">
                    <ul class="ma_g">
                        <li></li><li></li><li></li><li></li><li></li>
                    </ul>
                </div>
            </div>
        </div>
THINK;
		break;
	case 2:
		$pr20 = array($p[20] => "background-image:url($p[20])", $p[22] => "height:$p[22]px",);
		$p20 = $t->setStyle($pr20);
		$p20_hot = $t->setStyle(array($p[22] => "height:$p[22]px"));
		$p21_ai = array($p[21], $pst1);
		$p21_a = $t->setATag($p21_ai);
		$ps16 = $c->_r_sm_3($p[22], $p[22], '650') . 'px';
		$ps16_s = <<<THINK
        <div class="$p16_hide u_help ">
            <span>ͼƬ�ߴ�(��):1920px*$ps16</span>
            <span>��ǰģ��:��ҳ��ͼ�ֲ�</span>
        </div>
THINK;
		$p23 = $c->_r_sm($p[23] == 1, 'hot_h');
		$pr24 = array();
		$pr24Ip = $c->_r_sm($p[24], $c->ipStr2Arr2($p[24]));
		foreach ($pr24Ip as $k => $v) {
			$pr24i = array($v[0] . "0" => "background-image:url($v[0])", $v[1] . "1" => null, $v[2] . "2" => "left:$v[2]px", $v[3] . "3" => "top:$v[3]px", $v[4] . "4" => "width:$v[4]px", $v[5] . "5" => "height:$v[5]px;line-height:$v[5]px",);
			$p24s = $t->setStyle($pr24i);
			$pr24_a_i = array($v[1], $pst1);
			$pr24_a = $t->setATag($pr24_a_i);
			$p24_num = $k + 1;
			$pr24[] = "$pr24_a[0]<div class=\"hotDot hotDotView\" $p24s>�ȵ�{$p24_num}</div>$pr24_a[1]";
		}
		$p24 = implode($pr24);
		$ps2 = <<<THINK
{$p21_a[0]}
<div class="bgImage" {$p20}></div>{$p21_a[1]}
<div class="hot-box {$p23}" {$p20_hot}>{$p24}</div>{$p[25]}
THINK;
} ?>
<div class="tb-module tshop-um tshop-um-yy-Mfocus" <?= $ps0 ?>>
	<div class="box">
		<?= $ps16_s, $ps2 ?>
	</div>
</div>
