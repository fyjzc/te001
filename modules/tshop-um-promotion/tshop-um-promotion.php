<div class="tb-module tshop-um tshop-um-promotion">
	<?php
	global $c,$t,$srd,$uriManager,$id,$price,$discountPrice,$realPrice,$soldCount,$detailURI,$picUrl,$title;
	global $categoryIds,$items;
	//折扣价
	function discountPrice($item) {
		if ($item->discountPrice && $item->discountPrice != $item->price && $item->discountPrice != 0) {
			$discountPrice = number_format($item->discountPrice,3,".","");//格式化数字（取三位小数）
		} else {
			$discountPrice = NULL;
		}
		$discountPrice = substr($discountPrice,0,-1);//截取两位小数
		return $discountPrice;
	}

	function getRealItemsInfo($items,$itemNum,$i,$imgSize1,$imgSize2) {
		global $uriManager,$id,$price,$discountPrice,$realPrice,$soldCount,$detailURI,$picUrl,$title;
		if (count($items) < $itemNum && $i >= count($items)) {
			$i = 0;
		}
		if (!isset($items[$i])) {
			$id = 0;
			$price = 0;
			$discountPrice = 0;
			$realPrice = 0;
			$soldCount = 0;
			$detailURI = '#';
			$picUrl = 'https://img.alicdn.com/imgextra/i1/2667166845/TB2F3ZYkXXXXXaZXXXXXXXXXXXX_!!2667166845.png';
			$title = '没有找到宝贝,请先添加宝贝!';
		} else {
			$id = $items[$i]->id;
			$price = $items[$i]->price;
			$discountPrice = discountPrice($items[$i]);
			$realPrice = $discountPrice ? $discountPrice : $price;
			$soldCount = $items[$i]->soldCount;
			$detailURI = $uriManager->detailURI($items[$i]);
			$picUrl = $items[$i]->getPicUrl($imgSize1,$imgSize2);
			$title = $items[$i]->title;
		}
	}

	function getCategoryIdsOfSearch($in) {
		global $categoryIds,$c;
		$categoryLists = json_decode($in,TRUE);
		$categoryChildIds = NULL;
		foreach ($categoryLists as $categoryList) {
			if ($categoryList['rid']) {
				$categoryIds[] = $categoryList['rid'];
			}
			if ($categoryList['childIds']) {
				$categoryChildIds[] = $c->strEP($categoryList['childIds'],',');
			}
		}
		foreach ($categoryChildIds as $list) {
			foreach ($list as $id) {
				$categoryIds[] = $id;
			}
		}
		sort($categoryIds);
	}

	function getItemsOfCategorySearch($categoryIn,$sortType,$itemNum) {
		global $categoryIds,$itemManager,$items;
		$itemRowIds = NULL;
		getCategoryIdsOfSearch($categoryIn);
		foreach ($categoryIds as $idCount => $categoryId) {
			if ($categoryId) {
				$itemsList = $itemManager->queryByCategory($categoryId,$sortType,$itemNum);
				foreach ($itemsList as $itemObj) {
					$itemRowIds[] = $itemObj->id;
				}
			}
		}
		if ($itemRowIds) {
			$itemIds = array_flip(array_flip($itemRowIds));
			$items = $itemManager->queryByIds($itemIds,$sortType);
		}
	}

	$p = $c->inputIfoArr('p',50);
	$p_c = $c->inputIfoArr('c',50);
	$p0 = $c->strEP($p[0],'|');
	$p0r = array($srd->sMarginTop($p0[0]),$srd->sMarginBottom($p0[1]));
	$target = $p[1] ? $t->targetSet[0] : $t->targetSet[1];
	function inp7($num) {
		global $c,$p;
		$res = $c->isInChecks($num,$p[7]);
		return isset($res) ? TRUE : NULL;
	}

	?>
	<div class="box" <?= echoStyle($p0r) ?>>
		<? if (inp7(0)) { ?>
			<div class="topTitle"<?= echoStyle(array(
				$srd->sBgImage($p[8]),
				"font-family: 'Microsoft Yahei'",
			)) ?>>
				<? if (!$srd->isImage($p[8])) { ?>
					<span class="t_text">掌柜推荐</span>
				<? } ?>
			</div>
		<? } ?>
		<? if (inp7(9)) { ?>
			<div class="barBox">
				<div class="timer">
					<div class="J_TWidget" data-widget-type="Countdown"
						 data-widget-config="{'timeRunCls':'.tm_dates','timeEndCls':'.end','endTime':'<?= $p[27] ? $p[27] : 86400000 ?>','interval':1000,'timeUnitCls':{'d':'.ks-d','h':'.ks-h','m':'.ks-m','s':'.ks-s'},'minDigit':1}">
						<div class="tm_dates">
							<div class="timer_title" <?= echoStyle(array(
								$srd->sFontSize($p[24]),
								$srd->sColor($p[25]),$p[26],
								"font-family: 'Microsoft Yahei'",
							)) ?>><?= $p[23] ?></div>
							<span class="data">
						<span class="data_time ks-d"></span>
						<span class="data_text">天</span>
					</span>
							<span class="data">
						<span class="data_time ks-h"></span>
						<span class="data_text">时</span>
					</span>
							<span class="data">
						<span class="data_time ks-m"></span>
						<span class="data_text">分</span>
					</span>
							<span class="data">
						<span class="data_time ks-s"></span>
						<span class="data_text">秒</span>
					</span>
						</div>
						<span class="end"><?= $p[28] ?></span>
					</div>
				</div>
			</div>
		<? } ?>
		<?php
		$itemNum = 8;
		$picSize = 400;
		$sortType = $p[6];
		$items = $itemManager->queryByKeyword('',$sortType,$itemNum);
		switch ($p[3]) {
			case 1:
				$itemIds = $c->strEP($p[4],',');
				if (count($itemIds)) {
					$items = $itemManager->queryByIds($itemIds,$sortType);
				}
				break;
			case 2:
				if ($p[5]) {
					getItemsOfCategorySearch($p[5],$sortType,$itemNum);
				}
				break;
		}
		?>
		<div class="itemBox" <?= echoStyle(array($p_c[0])) ?>>
			<div class="imgInfo <?= $p[2] == 0 ? 'd_none' : NULL; ?>">
				<div class="textBox">
					<span class="text">当前模块:促销模块</span>
					<span class="text">当前图片尺寸:400px*400px</span>
				</div>
				<span class="bgc"></span>
			</div>
			<div class="J_TWidget" data-widget-type="Carousel"
				 data-widget-config="{'effect':'fade','easing':'easeNone','delay':0.1,'duration':0.5,'interval':3,'contentCls':'con','navCls':'nav','activeTriggerCls':'selected','circular':false,'autoplay':true}">
				<div class="con">
					<?php
					for ($i = 0;$i < $itemNum;$i++) {
						getRealItemsInfo($items,$itemNum,$i,$picSize,$picSize);
						?>
						<div class="itemLists">
							<div class="it_left">
								<div class="bgc"></div>
								<a href="<?= $detailURI ?>" <?= $target ?>></a>
								<img src="<?= $picUrl ?>">
							</div>
							<div class="it_right">
								<div class="it_detail">
									<? if (inp7(4) != NULL) { ?>
										<a href="<?= $detailURI ?>" <?= $target ?>>
											<span
												class="title" <?= echoStyle(array($srd->sFontSize($p[17]),$srd->sColor($p[18]),$p[19])) ?>><?= substr($title,0,26) . '...' ?></span>
										</a>
									<? } ?>
									<div class="sale_price">
										<?php if ($discountPrice && $discountPrice != $price && $discountPrice != 0) {
											if (inp7(1) != NULL) { ?>
												<div
													class="sale price" <?= echoStyle(array($srd->sColor($p[9]))) ?>>
													<span class="vt">原价:</span>
													<span
														class="price" <?= echoStyle(array($srd->sColor($p[10]))) ?>><?= $price ?></span>
													<? if (inp7(2) != NULL) { ?>
														<span class="through"></span>
													<? } ?>
												</div>
											<? }
										} ?>
										<? if (inp7(3) != NULL) { ?>
											<div class="sale n_prc">
												<span
													class="vt" <?= echoStyle(array($srd->sColor($p[12]),$p[13] == 1 ? 'font-weight:400' : NULL)) ?>><?= $p[11] ?></span>
												<span
													class="discountPrice" <?= echoStyle(array($srd->sColor($p[15]),$p[16] == 1 ? 'font-weight:400' : NULL)) ?>>
													<?php
													if ($p[14] == 0) {
														$realPrice = number_format($realPrice,2,'.','');
													}
													echo $realPrice
													?>
												</span>
											</div>
										<? }
										if (inp7(5) != NULL) { ?>
											<div class="sale" <?= echoStyle(array($srd->sColor($p[20]))) ?>>
												最近售出:&nbsp;<span
													class="num" <?= echoStyle(array($srd->sColor($p[21]),$p[22])) ?>><?= $soldCount ?></span>&nbsp;件
											</div>
										<? } ?>
									</div>
									<div class="buy_box">
										<? if (inp7(8) != NULL) { ?>
											<div class="tools">
												<div class="share tl" title="分享">
													<div class="sns-widget"
														 data-sharebtn='{type:"item",key:"<?= $id ?>",title:"<?= $title ?>",comment:"分享宝贝",pic:"",client_id:68,skinType:3,isShowFriend:false}'></div>
												</div>
												<div class="fav tl" title="收藏">
													<a href="https://favorite.taobao.com/popup/add_collection.htm?id=<?= $id ?>&itemtype=1&scjjc=1" <?= $target ?>></a>
												</div>
												<div class="code tl" title="二维码">
													<img class="code_img"
														 src="https://gqrcode.alicdn.com/img?type=ci&item_id=<?= $id ?>&v=1&w=90&h=90"/>
												</div>
												<div class="cart tl" title="加入购物车">
													<a href="<?= $detailURI ?>"
													   class="J_CartPluginTrigger" <?= $target ?>></a>
												</div>
											</div>
										<? } ?>
										<div class="buyBar">
											<? if (inp7(6) != NULL) { ?>
												<div class="add_cart">
													<a href="<?= $detailURI ?>"
													   class="J_CartPluginTrigger" <?= $target ?>><span
															class="b_text">加入购物车</span></a><span
														class="bgc"></span>
												</div>
											<? }
											if (inp7(7) != NULL) { ?>
												<div class="buy_now">
													<a href="<?= $detailURI ?>" <?= $target ?>><span
															class="b_text">立即购买</span></a><span
														class="bgc"></span>
												</div>
											<? } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<? } ?>
				</div>
				<div class="nav">
					<?php
					for ($i = 0;
						 $i < $itemNum;
						 $i++) {
						getRealItemsInfo($items,$itemNum,$i,400,400);
						?>
						<div class="nav_img<?= $i == 0 ? ' selected' : NULL ?>">
							<a href="<?= $detailURI ?>" <?= $target ?>></a>
							<img src="<?= $picUrl ?>">
						</div>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
</div>
