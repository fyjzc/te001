<?php
global $c, $srd, $uriManager;
$p = $c->inputIfoArr('p', 9);
$tp = $c->inputIfoArr('tp', 2);
$target = 'target="' . $p[1] . '"';
function borderColor($colorValue) {
	$s = new StyleRead();
	$value = trim($colorValue);
	return $s->isColor($value) ? 'border-color:' . $value : NULL;
}

?>
<div class="tb-module tshop-um tshop-um-NO14">
	<div class="box" <?= echoStyle(array($srd->sMarginBottom($p[0]))) ?>>
		<? if ($c->isInChecks(1, $tp[0])) { ?>
			<a <?= $target ?> class="sc" href="<?= $uriManager->favoriteLink() ?>" <?= echoStyle(array(
				borderColor($p[3]),
				$srd->sBgImage($p[2]),
			)) ?>></a>
		<? } ?>
		<? if ($c->isInChecks(2, $tp[0])) { ?>
			<div class="search">
				<div class="h-hd <?= $p[4] ? 'd_none' : '' ?>">
					<div class="h-bg">
						<span class="hd-txt"><?= $p[5] ?></span>
					</div>
				</div>
				<div class="h-bd" <?= echoStyle(array(borderColor($p[6]))) ?>>
					<div class="s_form">
						<form class="focus" name="SearchForm" action="<?= $uriManager->searchURI() ?>"
							  method="post" <?= $target ?>>
							<ul>
								<li class="keyword">
									<span class="h-label">�ؼ���</span>
									<input <?= echoStyle(array(borderColor($p[7]))) ?> type="text" size="16"
																					   name="keyword" class="text"
																					   value="" title="����ؼ���">
								</li>
								<li class="price">
									<span class="h-label">�۸�</span>
									<label><input <?= echoStyle(array(borderColor($p[7]))) ?> type="text"
																							  name="lowPrice"
																							  class="price-txt" size="4"
																							  value=""></label>
									<span class="connect-line">-</span>
									<label><input <?= echoStyle(array(borderColor($p[7]))) ?> type="text"
																							  name="highPrice"
																							  class="price-txt" size="4"
																							  value=""></label>
								</li>
								<li class="submit">
									<button <?= echoStyle(array(
										$srd->sColor($p[8]),
										$srd->sBgColor($p[9]),
									)) ?> type="submit" class="button" title="�ѵ��ڱ���">����
									</button>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		<? } ?>
	</div>
</div>
