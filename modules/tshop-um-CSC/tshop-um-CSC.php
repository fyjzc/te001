<?php
global $c, $srd;
$p = $c->inputIfoArr('p', 20);
$target = 'target="' . $p[1] . '"';
function borderColor($colorValue) {
	$s = new StyleRead();
	$value = trim($colorValue);
	return $s->isColor($value) ? 'border-color:' . $value : NULL;
}

?>
<div class="tb-module tshop-um tshop-um-CSC">
	<div class="box" <?= echoStyle(array($srd->sMarginBottom($p[0]))) ?>>
		<div class="h-hd <?= $p[2] ? 'd_none' : '' ?>">
			<div class="h-bg">
				<span class="hd-txt"><?= $p[3] ?></span>
			</div>
		</div>
		<div class="bot" <?= echoStyle(array(borderColor($p[4]))) ?>>
			<div class="bot-tip" <?= echoStyle(array($srd->sColor($p[8]))) ?>><?= $p[7] ?></div>
			<div class="bot-kf">
				<?php
				$sq_wwNicks = $c->strEP($p[14], '|');
				$sh_wwNicks = $c->strEP($p[18], '|');
				$sq_nicks = $c->strEP($p[15], '|');
				$sh_nicks = $c->strEP($p[19], '|');
				$sq_pic = $c->strEP($p[16], '|');
				$sh_pic = $c->strEP($p[20], '|');
				function pww($num, $wwNicks, $nicks, $pic, $fl = FALSE) {
					global $srd, $p, $uriManager, $_user;
					$nickStyle = echoStyle(array($srd->sColor($p[5])));
					$kf_class = $p[11] ? 'dkf' : 'kf';
					for ($i = 0; $i < $num; $i++) {
						$sellerNick = $wwNicks[$i] ? $wwNicks[$i] : $_user->nick;
						$wwNick = $uriManager->supportTag($sellerNick, "����ֱ�ӷ���", 2, $fl);
						$kf_tx = "<em{$nickStyle}>{$nicks[$i]}&nbsp;</em>{$wwNick}";
						$kfPicStyle = echoStyle(array($srd->sBgImage($pic[$i])));
						if ($p[11]) {
							$kf_tx = "{$wwNick}<div class=\"pic pic{$i}\" {$kfPicStyle}></div><em {$nickStyle}>{$nicks[$i]}</em>";
						}
						echo "<i class=\"{$kf_class} {$kf_class}{$i}\">{$kf_tx}</i>";
					}
				}

				$liNum = $p[11] ? 3 : 4;
				?>
				<div class="sq_kf">
					<p><?= $p[13] ?></p>
					<div class="kfh">
						<? $liNum = $liNum > count($sq_nicks) ? count($sq_nicks) : $liNum ?>
						<? pww($liNum, $sq_wwNicks, $sq_nicks, $sq_pic, $p[12]) ?>
					</div>
				</div>
				<div class="sh_kf">
					<p><?= $p[17] ?></p>
					<div class="kfh">
						<? $liNum = $liNum > count($sh_nicks) ? count($sh_nicks) : $liNum ?>
						<? pww($liNum, $sh_wwNicks, $sh_nicks, $sh_pic, $p[12]) ?>
					</div>
				</div>
			</div>
			<div class="bot-exp">
				<p class="k_time"><?= $p[9] ?></p>
				<p class="exp"><?= $p[10] ?></p>
			</div>
			<?= $p[6] ?>
		</div>
	</div>
</div>
