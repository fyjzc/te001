<?php
global $_user, $_shop, $shopCategoryManager, $shopManager, $uriManager, $itemManager, $c, $t, $srd;
?>
<div class="tb-module tshop-um tshop-um-topCate">
	<?php
	$p = $c->inputIfoArr('t', 50);
	$pl = $c->inputIfoArr('tl', 50);
	$pc = $c->inputIfoArr('tc', 50);
	$p0s = $c->strEP($p[0], '|');
	$target = ' target=' . $p[1];
	?>
	<div class="box" <?= echoStyle(array(
		$srd->sMarginTop($p0s[0]),
		$srd->sMarginBottom($p0s[1]),
	)) ?>>
		<div class="mainBox">
			<div class="categoryBox">
				<div class="cateTitle">
					<div class="t_tBox">
						<span class="t_textCN" <?= echoStyle(array(
							"font-family:'Microsoft YaHei'",
						)) ?>>热卖品类</span>&nbsp;<span class="t_textEN" <?= echoStyle(array(
							"font-family:Arial",
						)) ?>>CATEGORY</span>
					</div>
					<div class="searchBox">
						<div class="label">热门搜索</div>
						<form class="search_form" <?= $target ?> action="<?= $uriManager->searchURI() ?>"
							  method="post">
							<input class="keyword" type="text" name="keyword" value="" title="请输入关键字">
							<input class="submit" type="submit" value="" title="搜索">
							<div class="sImg"></div>
						</form>
					</div>
				</div>
				<div class="cateBody">
					<?php
					$maxNumC = 6;
					$maxNumS = 5;
					$categories = $shopCategoryManager->queryAll();
					$cTNames1 = NULL;
					$cTNames2 = NULL;
					$cTLinks1 = NULL;
					$cTLinks2 = NULL;
					switch ($p[2]) {
						case 2:
							$categoriesIds = json_decode($p[3], TRUE);
							if (count($categoriesIds) > 0) {
								foreach ($categoriesIds as $categoriesId) {
									$categoriesRids[] = $shopCategoryManager->queryById($categoriesId['rid']);
								}
								$categories = $categoriesRids;
							}
							break;
						case 3:
							$cTNames1 = array(
								$c->strEP($pl[2], ',')[0],
								$c->strEP($pl[5], ',')[0],
								$c->strEP($pl[8], ',')[0],
								$c->strEP($pl[11], ',')[0],
								$c->strEP($pl[14], ',')[0],
								$c->strEP($pl[17], ',')[0],
							);
							$cTNames2 = array(
								$c->strEP($pl[3], ','),
								$c->strEP($pl[6], ','),
								$c->strEP($pl[9], ','),
								$c->strEP($pl[12], ','),
								$c->strEP($pl[15], ','),
								$c->strEP($pl[18], ','),
							);
							$cTLinks1 = array(
								$c->strEP($pl[2], ',')[1],
								$c->strEP($pl[5], ',')[1],
								$c->strEP($pl[8], ',')[1],
								$c->strEP($pl[11], ',')[1],
								$c->strEP($pl[14], ',')[1],
								$c->strEP($pl[17], ',')[1],
							);
							$cTLinks2 = array(
								$c->strEP($pl[4], ','),
								$c->strEP($pl[7], ','),
								$c->strEP($pl[10], ','),
								$c->strEP($pl[13], ','),
								$c->strEP($pl[16], ','),
								$c->strEP($pl[19], ','),
							);
							break;
					}
					$cTs = $c->strEP($pl[1], ',');
					for ($i = 0; $i < $maxNumC; $i++) {
						if ($categories[$i]->id || isset($cTNames1[$i])) {
							$subCategories = $shopCategoryManager->querySubCategories($categories[$i]->id); ?>
							<div class="cateList cateList<?= $i ?>">
								<a <?= $target ?>
									href="<?= isset($cTLinks1[$i]) ? $cTLinks1[$i] : $uriManager->shopCategoryURI($categories[$i]) ?>">
									<span class="cateName" <?= echoStyle(array(
										"font-family:'Microsoft YaHei'",
									)) ?>><?= isset($cTNames1[$i]) ? $cTNames1[$i] : $categories[$i]->name ?></span>
									<span class="cateName_en" <?= echoStyle(array(
										$pl[0],
										"font-family:Arial",
									)) ?>><?= $cTs[$i] ?></span>
								</a>
								<ul><?php
									for ($y = 0; $y < $maxNumS; $y++) {
										if ($subCategories[$y]->id || isset($cTNames2[$i][$y])) { ?>
											<li class="subCatList">
												<a <?= $target ?>
													href="<?= isset($cTLinks2[$i][$y]) ? $cTLinks2[$i][$y] : $uriManager->shopCategoryURI($subCategories[$y]) ?>">
												<span class="subCateName" <?= echoStyle(array(
													"font-family:'Microsoft YaHei'",
												)) ?>><?= isset($cTNames2[$i][$y]) ? $cTNames2[$i][$y] : $subCategories[$y]->name ?></span>
												</a>
											</li>
										<? }
									} ?>
								</ul>
							</div>
						<? }
					} ?>
				</div>
			</div>
			<div class="rPic"></div>
			<div class="customerBox">
				<?php
				$pc3 = $c->strEP($pc[3], '|');
				$pc4 = $c->strEP($pc[4], '|');
				$pc5 = $c->strEP($pc[5], '|');
				$pc7 = $c->strEP($pc[7], '|');
				$pc8 = $c->strEP($pc[8], '|');
				$pc9 = $c->strEP($pc[9], '|');
				?>
				<div class="cImg"></div>
				<div class="s_left"></div>
				<div class="title1"><span><?= $pc[2] ?></span></div>
				<div class="kf_ww">
					<? switch ($pc[0]) {
						case 0:
							for ($i = 0; $i < 5; $i++) {
								$customerID1 = $pc4[$i * 2] ? $pc4[$i * 2] : $_user->nick;
								$customerID2 = $pc4[$i * 2 + 1] ? $pc4[$i * 2 + 1] : $_user->nick;
								?>
								<div class="ww">
									<div class="wwm">
										<span class="ww_nc ww_ncm"><?= $pc3[$i * 2] ?></span>
										<?= $uriManager->supportTag($customerID1, "联系客服", 2, $pc[1]); ?>
									</div>
									<div class="wwm">
										<span class="ww_nc ww_ncm"><?= $pc3[$i * 2 + 1] ?></span>
										<?= $uriManager->supportTag($customerID2, "联系客服", 2, $pc[1]); ?>
									</div>
								</div>
							<? } ?>
							<? break; ?>
						<? case 1:
							for ($i = 0; $i < 2; $i++) {
								$customerID1 = $pc4[$i * 2] ? $pc4[$i * 2] : $_user->nick;
								$customerID2 = $pc4[$i * 2 + 1] ? $pc4[$i * 2 + 1] : $_user->nick;
								?>
								<div class="wwBox wwBox1">
									<div class="wwb">
										<span class="ww_ncb"><?= $pc3[$i * 2] ?></span>
										<?= $uriManager->supportTag($customerID1, "联系客服", 1, $pc[1]); ?>
									</div>
									<div class="wwb">
										<span class="ww_ncb"><?= $pc3[$i * 2 + 1] ?></span>
										<?= $uriManager->supportTag($customerID1, "联系客服", 1, $pc[1]); ?>
									</div>
								</div>
							<? } ?>
							<? break; ?>
						<? case 2:
							for ($i = 0; $i < 5; $i++) { ?>
								<div class="ww ww<?= $i ?>">
									<div class="ww_pic" <?= echoStyle(array(
										$srd->sBgImage($pc5[$i]),
									)) ?>>
										<?= $uriManager->supportTag($pc4[$i] ? $pc4[$i] : $_user->nick, "联系客服", 2, $pc[1]); ?>
									</div>
									<div class="ww_nc"><?= $pc3[$i] ?></div>
								</div>
							<? } ?>
							<? break; ?>
						<? } ?>
				</div>
				<div class="title1"><span><?= $pc[6] ?></span></div>
				<div class="kf_ww kf2">
					<?php switch ($pc[0]) {
						case 0:
							for ($i = 0; $i < 2; $i++) {
								$customerID1 = $pc8[$i * 2] ? $pc8[$i * 2] : $_user->nick;
								$customerID2 = $pc8[$i * 2 + 1] ? $pc8[$i * 2 + 1] : $_user->nick;
								?>
								<div class="ww">
									<div class="wwm">
										<span class="ww_nc ww_ncm"><?= $pc7[$i * 2] ?></span>
										<?= $uriManager->supportTag($customerID1, "联系客服", 2, $pc[1]); ?>
									</div>
									<div class="wwm">
										<span class="ww_nc ww_ncm"><?= $pc7[$i * 2 + 1] ?></span>
										<?= $uriManager->supportTag($customerID2, "联系客服", 2, $pc[1]); ?>
									</div>
								</div>
							<? } ?>
							<? break; ?>
						<? case 1: ?>
							<div class="wwBox">
								<div class="wwb">
									<span class="ww_ncb"><?= $pc7[0] ?></span>
									<?= $uriManager->supportTag($pc8[0] ? $pc8[0] : $_user->nick, "联系客服", 1, $pc[1]); ?>
								</div>
								<div class="wwb">
									<span class="ww_ncb"><?= $pc7[1] ?></span>
									<?= $uriManager->supportTag($pc8[1] ? $pc8[1] : $_user->nick, "联系客服", 1, $pc[1]); ?>
								</div>
							</div>
							<? break; ?>
						<? case 2: ?>
							<? for ($i = 0; $i < 2; $i++) { ?>
								<div class="ww ww<?= $i + 5 ?>">
									<div class="ww_pic" <?= echoStyle(array(
										$srd->sBgImage($pc9[$i]),
									)) ?>>
										<?= $uriManager->supportTag($pc8[$i] ? $pc8[$i] : $_user->nick, "联系客服", 2, $pc[1]); ?>
									</div>
									<div class="ww_nc"><?= $pc7[$i] ?></div>
								</div>
							<? } ?>
							<? break; ?>
						<? } ?>
				</div>
				<div class="title1"><span>在线时间</span></div>
				<div class="onTime"><span>09:00AM-24:00PM</span></div>
				<div class="borderBox"></div>
			</div>
		</div>
		<div class="state_gg state_line1"></div>
		<div class="state_gg state_line2"></div>
		<div class="state_gg state_line3"></div>
		<div class="state_gg state_line4"></div>
		<div class="state_gg state_line5"></div>
		<div class="state_gg state_line6"></div>
	</div>
</div>
