<?php
global $regionWidth, $c, $t, $srd, $uriManager, $itemManager, $id, $price, $discountPrice, $realPrice, $soldCount, $detailURI, $picUrl, $title;
function discountPrice($item) {
	if ($item->discountPrice && $item->discountPrice != $item->price && $item->discountPrice != 0) {
		$discountPrice = number_format($item->discountPrice, 3, ".", "");//格式化数字（取三位小数）
	} else {
		$discountPrice = NULL;
	}
	$discountPrice = substr($discountPrice, 0, -1);//截取两位小数
	return $discountPrice;
}

function getRealItemsInfo($items, $itemNum, $i, $imgSize1, $imgSize2) {
	global $uriManager, $id, $price, $discountPrice, $realPrice, $soldCount, $detailURI, $picUrl, $title;
	if (count($items) < $itemNum && $i >= count($items)) {
		$i = 0;
	}
	if (!isset($items[$i])) {
		$id = 0;
		$price = 0;
		$discountPrice = 0;
		$realPrice = 0;
		$soldCount = 0;
		$detailURI = '#';
		$picUrl = 'https://img.alicdn.com/imgextra/i1/2667166845/TB2F3ZYkXXXXXaZXXXXXXXXXXXX_!!2667166845.png';
		$title = '没有找到宝贝,请先添加宝贝!';
	} else {
		$id = $items[$i]->id;
		$price = $items[$i]->price;
		$discountPrice = discountPrice($items[$i]);
		$realPrice = $discountPrice ? $discountPrice : $price;
		$soldCount = $items[$i]->soldCount;
		$detailURI = $uriManager->detailURI($items[$i]);
		$picUrl = $items[$i]->getPicUrl($imgSize1, $imgSize2);
		$title = $items[$i]->title;
	}
}

/**
 * //通过获取输入的JSON商品类目对象,最终获得不重复的商品对象
 *
 * @param $selectCategoryIn
 * @param $sortType
 * @param $count
 *
 * @return array
 * //获取类目选择器选择的类目,生成不重复的所有类目宝贝的ID数组;然后查询
 */
function getItemsFromCategoryS($selectCategoryIn, $sortType, $count) {
	global $itemManager;
	$categoryIds = array();
	$itemsTs = array();
	$itemsAll = array();
	$itemIds = array();
	$items = array();
	if ($selectCategoryIn) {
		$categoryArr = json_decode($selectCategoryIn, TRUE);
		foreach ($categoryArr as $ids) {
			if ($ids['childIds'] == '') {
				$categoryIds[] = $ids['rid'];
			} else {
				$childIds = explode(',', $ids['childIds']);
				foreach ($childIds as $childId) {
					$categoryIds[] = $childId;
				}
			}
		}
		foreach ($categoryIds as $categoryId) {
			$itemsTs[] = $itemManager->queryByCategory($categoryId, $sortType, 20);
		}
	}
	if (count($itemsTs) > 0) {
		foreach ($itemsTs as $itemsT) {
			foreach ($itemsT as $item) {
				$itemsAll[] = $item;
			}
		}
	}
	if (count($itemsAll) > 0) {
		foreach ($itemsAll as $item) {
			$itemIds[] = $item->id;
		}
		$itemIds_tmp = array_flip(array_flip($itemIds));
		$itemsQs = $itemManager->queryByIds($itemIds_tmp, $sortType);
		for ($i = 0; $i < $count; $i++) {
			$items[$i] = $itemsQs[$i];
		}
	}
	return $items;
}

?>
<div class="tb-module tshop-um tshop-um-itemSyn">
	<?php
	$p = $c->inputIfoArr('s', 10);
	$plb = $c->inputIfoArr('lb', 10);
	$pbb = $c->inputIfoArr('bb', 10);
	$ps0 = $c->strEP($p[0], '|');
	$target = 'target="' . $p[1] . '"';
	$plbS1 = $c->strEP($plb[1], '|');
	$plbUris1 = $c->strEP($plb[2], '|');
	$lbNum = count($plbS1) > 0 ? count($plbS1) : 2;
	?>
	<div
		class="box box<?= $regionWidth ?>" <?php
	if ($regionWidth == 950) {
		echo echoStyle(array($srd->sMarginTop($ps0[0]), $srd->sMarginBottom($ps0[1]),));
	}
	?>>
		<? if ($c->isInChecks(1, $p[2])): ?>
			<div class="lb1" <?= echoStyle(array($srd->sHeight($plb[0]))) ?>>
				<div class="J_TWidget LBox" style="" data-widget-type="Carousel"
					 data-widget-config="{'autoplay':true , 'effect':'scrollx','easing':'easeBoth','duration':0.8,'interval':5,'contentCls':'carouselCon','navCls': 'carouselNav','prevBtnCls':'prev','nextBtnCls':'next','circular':true,'activeIndex':0,'activeTriggerCls':'selected'}">
					<div class="con" <?= echoStyle(array($srd->sHeight($plb[0]))) ?>>
						<ul class="carouselCon">
							<? for ($i = 0; $i < $lbNum; $i++) { ?>
								<li class="pic pic<?= $i ?>" <?= echoStyle(array($srd->sBgImage($plbS1[$i]))) ?>><a
										<?= echoStyle(array($srd->sHeight($plb[0]))) ?>
										href="<?= $plbUris1[$i] ? $plbUris1[$i] : '#' ?>" <?= $target ?>></a></li>
							<? } ?>
						</ul>
					</div>
					<div class="cNav">
						<ul class="carouselNav">
							<? for ($i = 0; $i < $lbNum; $i++) { ?>
								<li class="btn <?= $i == 0 ? 'selected' : NULL ?>"></li>
							<? } ?>
						</ul>
					</div>
				</div>
			</div>
		<? endif; ?>
		<?php
		$imgCenter = 'background-position: center center';
		$font_m = 'font-family:\'微软雅黑\'';
		$sortType = $pbb[1];
		$maxItemsNum = 5;
		$items = $itemManager->queryByKeyword('', $sortType, $maxItemsNum);
		switch ($pbb[0]) {
			case 2:
				$items = getItemsFromCategoryS($pbb[2], $sortType, $maxItemsNum);
				break;
			case 3:
				$items = $itemManager->queryByIds($c->strEP($pbb[3], ','), $sortType);
				break;
		}
		?>
		<? if ($c->isInChecks(2, $p[2])) { ?>
			<? if ($regionWidth == 950) { ?>
				<div class="itemBox1">
					<div class="iBox">
						<? for ($i = 0; $i < $maxItemsNum; $i++) {
							$picSize = $i == 0 && $p[3] != 3 ? 570 : 290;
							getRealItemsInfo($items, $maxItemsNum, $i, $picSize, $picSize);
							$imgBoxStyle = NULL;
							$imgInfoStyle = echoStyle(array(
								$srd->sBgImageO($picUrl),
								$imgCenter,
							));
							if ($p[3] == 2) {
								$imgBoxStyle = echoStyle(array(
									'float:right',
									$srd->sMarginLeft(0),
									$srd->sMarginRight($i == 0 ? 0 : 20),
								));
								$imgInfoStyle = echoStyle(array(
									$srd->sBgImageO($picUrl),
									$imgCenter,
								));
							} elseif ($p[3] == 3) {
								$imgBoxStyle = echoStyle(array(
									$srd->sMarginLeft($i == 0 || $i == 3 ? 0 : 40),
								));
								$imgInfoStyle = echoStyle(array(
									$srd->sBgImageO($picUrl),
									$imgCenter,
									$srd->sWidth(290),
									$srd->sHeight(240),
								));
							} ?>
							<div class="imgBox img<?= $i ?>" <?= $imgBoxStyle; ?>>
								<div class="t_img">
									<a href="<?= $detailURI ?>" <?= $target ?>
									   class="imgInfo" <?= $imgInfoStyle ?>>
										<div class="h_mark"></div>
									</a>
								</div>
								<div class="t_btn">
									<a class="buy_btn"
									   href="<?= $detailURI ?>" <?= $target ?> <?= echoStyle(array($font_m)) ?>> <span
											class="cn">立即购买</span> <span class="en">BUY NOW ></span> </a>
									<span class="h_price" <?= echoStyle(array('font-family:Arial')) ?>>
										<span class="symbol">￥</span>
										<span class="num"><?= $realPrice ?></span>
									</span>
								</div>
							</div>
						<? } ?>
					</div>
				</div>
			<? } ?>
		<? } ?>
		<? if ($c->isInChecks(3, $p[2]) && $regionWidth == 950) { ?>
			<?php
			$plbS2 = $c->strEP($plb[4], '|');
			$plbUris2 = $c->strEP($plb[5], '|');
			$lbNum2 = count($plbS2) > 0 ? count($plbS2) : 2;
			?>
			<div class="lb1 lb1_2" <?= echoStyle(array($srd->sHeight($plb[3]))) ?>>
				<div class="J_TWidget LBox" style="" data-widget-type="Carousel"
					 data-widget-config="{'autoplay':true , 'effect':'scrollx','easing':'easeBoth','duration':0.8,'interval':5,'contentCls':'carouselCon','navCls': 'carouselNav','prevBtnCls':'prev','nextBtnCls':'next','circular':true,'activeIndex':0,'activeTriggerCls':'selected'}">
					<div class="con" <?= echoStyle(array($srd->sHeight($plb[3]))) ?>>
						<ul class="carouselCon">
							<? for ($i = 0; $i < $lbNum2; $i++) { ?>
								<li class="pic pic<?= $i ?>" <?= echoStyle(array($srd->sBgImage($plbS2[$i]))) ?>><a
										<?= echoStyle(array($srd->sHeight($plb[3]))) ?>
										href="<?= $plbUris2[$i] ? $plbUris2[$i] : '#' ?>" <?= $target ?>></a></li>
							<? } ?>
						</ul>
					</div>
					<div class="cNav">
						<ul class="carouselNav">
							<? for ($i = 0; $i < $lbNum2; $i++) { ?>
								<li class="btn <?= $i == 0 ? 'selected' : NULL ?>"></li>
							<? } ?>
						</ul>
					</div>
				</div>
			</div>
		<? } ?>
		<?php
		$sortType2 = $pbb[5];
		$maxItemsNum2 = $p[4] == 3 ? 6 : 5;
		$items2 = $itemManager->queryByKeyword('', $sortType2, $maxItemsNum2);
		switch ($pbb[4]) {
			case 2:
				$items2 = getItemsFromCategoryS($pbb[6], $sortType2, $maxItemsNum2);
				break;
			case 3:
				$items2 = $itemManager->queryByIds($c->strEP($pbb[7], ','), $sortType2);
				break;
		}
		?>
		<? if ($c->isInChecks(4, $p[2])) { ?>
			<? if ($regionWidth == 950) { ?>
				<div class="itemBox1 itemBox2">
					<div class="iBox">
						<? for ($i = 0; $i < $maxItemsNum2; $i++) {
							$picSize = $i == 0 && $p[4] != 3 ? 570 : 290;
							getRealItemsInfo($items2, $maxItemsNum2, $i, $picSize, $picSize);
							$imgBoxStyle = NULL;
							$imgInfoStyle = echoStyle(array(
								$srd->sBgImageO($picUrl),
								$imgCenter,
							));
							if ($p[4] == 2) {
								$imgBoxStyle = echoStyle(array(
									'float:right',
									$srd->sMarginLeft(0),
									$srd->sMarginRight($i == 0 ? 0 : 20),
								));
								$imgInfoStyle = echoStyle(array(
									$srd->sBgImageO($picUrl),
									$imgCenter,
								));
							} elseif ($p[4] == 3) {
								$imgBoxStyle = echoStyle(array(
									$srd->sMarginLeft($i == 0 || $i == 3 ? 0 : 40),
								));
								$imgInfoStyle = echoStyle(array(
									$srd->sBgImageO($picUrl),
									$imgCenter,
									$srd->sWidth(290),
									$srd->sHeight(240),
								));
							} ?>
							<div class="imgBox img<?= $i ?>" <?= $imgBoxStyle; ?>>
								<div class="t_img">
									<a href="<?= $detailURI ?>" <?= $target ?>
									   class="imgInfo" <?= $imgInfoStyle ?>>
										<div class="h_mark"></div>
									</a>
								</div>
								<div class="t_btn">
									<a class="buy_btn"
									   href="<?= $detailURI ?>" <?= $target ?> <?= echoStyle(array($font_m)) ?>> <span
											class="cn">立即购买</span> <span class="en">BUY NOW ></span> </a>
									<span class="h_price" <?= echoStyle(array('font-family:Arial')) ?>>
										<span class="symbol">￥</span>
										<span class="num"><?= $realPrice ?></span>
									</span>
								</div>
							</div>
						<? } ?>
					</div>
				</div>
			<? } ?>
		<? } ?>
	</div>
</div>
