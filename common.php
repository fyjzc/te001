<?php

class Tags
{
	public $url = "https://www.taobao.com";
	public $target = array('_blank','_self');
	public $targetSet = array(' target="_blank"',' target="_self"');

	function setATag(array $_aTop,$_aEnd = "</a>") {
		global $c;
		$tagAs = array();
		if ($_aTop[0]) {//如果不存在链接地址,这个a标签就销毁了;
			$aTop1 = $c->_r_sm($_aTop[1]," target=\"{$_aTop[1]}\"");
			$aTop2 = $c->_r_sm($_aTop[2]," class=\"{$_aTop[2]}\"");//class 字符串
			$aTop3 = $c->_r_sm($_aTop[3]," style=\"{$_aTop[3]};\"");
			$tagAs[0] = "<a href=\"{$_aTop[0]}\" {$aTop1}{$aTop2}{$aTop3}>";
			$tagAs[1] = $_aEnd;
		}
		return $tagAs;
	}

	function setStyle(array $_style) {
		$arr = array();
		$style = NULL;
		$i = 0;
		foreach ($_style as $k => $v) {
			$n = str_replace($i,NULL,$k);
			if ($n && $v) {
				$arr[$i] = $v;
			};
			$i++;
		}
		if (implode($arr)) {
			$style = " style=\"" . implode(';',$arr) . ";\"";
		}
		return $style;
	}

	function setStyles($inputArr) {
		$arr = array();
		$style = NULL;
		$i = 0;
		foreach ($inputArr as $values) {
			if ($values[0] && $values[1]) {
				$arr[$i] = $values[1];
				$i++;
			}
		}
		if (implode($arr)) {
			$style = " style=\"" . implode(';',$arr) . ";\"";
		}
		return $style;
	}
}

class CustomNew
{
	function inputIfoArr($m,$in) {
		global $_MODULE;
		$p = array();
		for ($i = 0;$i <= $in;$i++) {
			$p[$i] = $_MODULE[$m . $i];
		}
		return $p;
	}

	function ipStr2Arr2($inputStr,$firstDelimiter = ',',$secondDelimiter = '|') {
		$arrInput = array();
		$arr = explode($firstDelimiter,trim($inputStr));
		foreach ($arr as $key => $value) {
			$arrIp = explode($secondDelimiter,$value);
			if ($arrIp) $arrInput[$key] = $arrIp;
		}
		return $arrInput;
	}

	function _r_sm($b,$_str) {
		return trim($b) ? $_str : NULL;
	}

	function _r_sm_3($b,$_str,$_rs) {
		return trim($b) ? $_str : $_rs;
	}

	function strEP($_str,$_dl) {
		return trim($_str) ? explode($_dl,$_str) : NULL;
	}

	function check_arr($str_check,$dt = "@_@") {
		return explode($dt,$str_check);
	}

	function isInChecks($checkNum,$checkStr) {
		$check_arr = $this->check_arr($checkStr);
		return in_array($checkNum,$check_arr) ? $checkNum : NULL;
	}
}

class StyleRead
{
	function exist($variable) {
		$variable = trim($variable);
		return ($variable == '' || !isset($variable)) ? FALSE : TRUE;
	}

	function isInt($paramNum) {
		return ($this->exist($paramNum) && $paramNum == intval($paramNum) . '') ? TRUE : FALSE;
	}

	function isColor($paramColor) {
		if ($this->exist($paramColor)) {
			preg_match_all('/#/',$paramColor,$count);
			$strLen = strlen($paramColor);
			$value = str_replace('#','',$paramColor);
			if (is_numeric(hexdec($value)) && $paramColor[0] == '#' && count($count[0]) == 1 && ($strLen == 4 || $strLen == 7)) {
				return TRUE;
			}
		}
		return FALSE;
	}

	function isUrl($paramUrl) {
		if ($this->exist($paramUrl) && ((strpos($paramUrl,'http://') === 0) || (strpos($paramUrl,'https://') === 0))) {
			return TRUE;
		}
		return FALSE;
	}

	function isImage($imageUrl) {
		if ($this->exist($imageUrl) && $this->isUrl($imageUrl) && (strpos($imageUrl,'.png') || strpos($imageUrl,'.jpg') || strpos($imageUrl,'.gif'))) {
			return TRUE;
		}
		return FALSE;
	}

	function sBgImage($imageUrl) {
		$value = trim($imageUrl);
		return $this->isImage($value) ? 'background:url(' . $value . ') 0 0 no-repeat' : NULL;
	}

	function sBgImageO($imageUrl) {
		$value = trim($imageUrl);
		return 'background-image:url(https:' . $value . ')';
	}

	function sImage($imageUrlValue) {
		$value = trim($imageUrlValue);
		return $this->isImage($value) ? $value : NULL;
	}

	function sUrl($urlValue) {
		$value = trim($urlValue);
		return $this->isUrl($value) ? $value : NULL;
	}

	function sBgColor($colorValue) {
		$value = trim($colorValue);
		return $this->isColor($value) ? 'background-color:' . $value : NULL;
	}

	function sColor($colorValue) {
		$value = trim($colorValue);
		return $this->isColor($value) ? 'color:' . $value : NULL;
	}

	function sFontSize($sizeV) {
		$value = trim($sizeV);
		return $this->isInt($value) ? 'font-size:' . $value . 'px' : NULL;
	}

	function sTop($topValue) {
		$value = trim($topValue);
		return $this->isInt($value) ? 'top:' . $value . 'px' : NULL;
	}

	function sLeft($leftValue) {
		$value = trim($leftValue);
		return $this->isInt($value) ? 'left:' . $value . 'px' : NULL;
	}

	function sRight($rightValue) {
		$value = trim($rightValue);
		return $this->isInt($value) ? 'right:' . $value . 'px' : NULL;
	}

	function sBottom($bottomValue) {
		$value = trim($bottomValue);
		return $this->isInt($value) ? 'bottom:' . $value . 'px' : NULL;
	}

	function sMarginTop($marginTopValue) {
		$value = trim($marginTopValue);
		return $this->isInt($value) ? 'margin-top:' . $value . 'px' : NULL;
	}

	function sMarginLeft($marginLeftValue) {
		$value = trim($marginLeftValue);
		return $this->isInt($value) ? 'margin-left:' . $value . 'px' : NULL;
	}

	function sMarginRight($marginRightValue) {
		$value = trim($marginRightValue);
		return $this->isInt($value) ? 'margin-right:' . $value . 'px' : NULL;
	}

	function sMarginBottom($marginBottomValue) {
		$value = trim($marginBottomValue);
		return $this->isInt($value) ? 'margin-bottom:' . $value . 'px' : NULL;
	}

	function sWidth($widthValue) {
		$value = trim($widthValue);
		return $this->isInt($value) ? 'width:' . $value . 'px' : NULL;
	}

	function sHeight($heightValue) {
		$value = trim($heightValue);
		return $this->isInt($value) ? 'height:' . $value . 'px' : NULL;
	}
}

$srd = new StyleRead();
$c = new CustomNew();
$t = new Tags();
function sT1($input,$style) {
	return array($input == '0' ? 1 : $input,$style);
}

function echoStyle($styleStrings) {
	$srr = NULL;
	foreach ($styleStrings as $styleString) {
		if (isset($styleString)) $srr[] = $styleString;
	}
	return implode($srr) ? ' style="' . implode(';',$srr) . ';"' : NULL;
}
?>